import controller.BitString;
import controller.PoemGenerator;
import controller.exception.InvalidBitStringException;

public class AutomatedPoetry {

	/**
	 * The main method of the EvoPoem poetry generator
	 * Generates a few poems based on bit strings and prints
	 * out those poems
	 * @param args	<not used>
	 */
	public static void main(String[] args) {
		PoemGenerator genX = new PoemGenerator();

		System.out.println("Started");
		br();

		try {
			BitString b1 = new BitString("1111111111111111");
			BitString b2 = new BitString("0000000000000000");
			BitString b3 = new BitString("0111000111100011");
			BitString b4 = new BitString(
					"101010011101011010100111010100110110011000101010100100000011001111100010011100010101100001001111111000100010010011100100110001011000100110101");
			System.out.println(genX.GeneratePoem(b1));
			br();
			System.out.println(genX.GeneratePoem(b2));
			br();
			System.out.println(genX.GeneratePoem(b3));
			br();
			System.out.println(genX.GeneratePoem(b4));
			br();
			printXRandomPoems(genX, 5);
		} catch (InvalidBitStringException e) {
			e.printStackTrace();
		}

		System.out.println("Ended");
		System.exit(0);
	}
	
	/**
	 * Method to print out a new line
	 */
	private static void br() {
		System.out.println("\n");
	}
	
	/**
	 * Method to create random bitstrings and generate poems from those bitstrings
	 * @param genX	PoemGenerator object
	 * @param x		Amount of random bit strings to generate
	 */
	private static void printXRandomPoems(PoemGenerator genX, int x) {
		for (int i = 0; i < x; i++) {
			System.out.println(genX.GeneratePoem(BitString.random()));
			br();
		}
	}
}
