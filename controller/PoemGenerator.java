package controller;

import java.util.ArrayList;
import java.util.LinkedList;

import controller.exception.FeatureTypesDontMatchException;
import controller.exception.InvalidFeatureValueTypeException;
import controller.exception.UnknownClauseTypeException;
import model.grammar.Clause;
import model.grammar.Grammar;
import model.grammar.feature.AttributeValueMatrix;
import model.poems.Poem;
import model.word.Word;
import model.wordlist.WordList;

/**
 * The PoemGenerator class deterministically generates poetry from a bitstring, 
 * using a Definite Clause Grammar, lexicon and feature value matrix
 */
public class PoemGenerator {

	/**
	 * Generates a poem from the bit string, using the grammar and word lists
	 * In this implementation, exactly four lines are generated very time
	 * @param bitString		The bit string for this poem
	 * @return 				A poem object
	 */
	public Poem GeneratePoem(BitString bitstring) {
		Poem poem = new Poem();
		BitStringIterator iterator = (BitStringIterator) bitstring.iterator();
		
		for(int i = 0; i < 4; i++)
		{
			generateLine(poem, iterator);
		}
		
		return poem;
	}
	
	/**
	 * Method that generates a single line for the poem
	 * @param poem			The poem object to be filled in
	 * @param iterator		The iterator for the bitstring
	 */
	private void generateLine(Poem poem, BitStringIterator iterator)
	{
		LinkedList<Word> words;
		
		try{
			Clause skeleton = buildSkeleton(new Grammar().get(), iterator);
			skeleton = unifyAvms(skeleton);
			skeleton.getAvm().fixAllDomains(iterator);
			words = fillGrammar(skeleton, iterator);
			LinkedList<String> line = clause2wordList(words, skeleton.getAvm(), iterator);
			int ident = iterator.next() % 4;
			poem.addLine(line, ident);
		} catch (UnknownClauseTypeException e)
		{
			e.printStackTrace();
			words = new LinkedList<Word>();
		}
		
	}
	
	/**
	 * Transforms a Clause object into a list of words, using the bit string
	 * @param grammar		Grammar object with all possible grammar combinations
	 * @param iterator		Bit String iterator that returns next integer
	 * @return				Clause containing only word lists
	 * @throws UnknownClauseTypeException
	 * 						When a Clause is requested that does not exist
	 */
	private Clause buildSkeleton(Clause grammar, BitStringIterator iterator) 
			throws UnknownClauseTypeException
	{
		boolean allWords;
		do {
			allWords = true;
			for(int j = 0; j < grammar.getSize(); j++)
			{
				Clause item = grammar.get(j);
				if(item.getType() == Clause.GROUP)
				{
					int b = iterator.next() % item.getSize();
					Clause replacement = item.get(b);
					grammar.replace(j, replacement);
					allWords &= replacement.getType() == Clause.LEXICON;
				} else if (item.getType() == Clause.CLAUSE) {
					grammar.replace(j, item.getElements());
					for(Clause a : item.getElements())
					{
						allWords &= a.getType() == Clause.LEXICON;
					}
				} else if (item.getType() == Clause.LEXICON) {
					continue;
				} else {
					throw new UnknownClauseTypeException();
				}
			}
		} while(!allWords);
		return grammar;
	}
	
	/**
	 * Method to unify all AVMs in the skeleton
	 * @param skeleton		The poem skeleton (as clause object)
	 * @return				The clause with all AVMs unified
	 */
	private Clause unifyAvms(Clause skeleton)
	{
		skeleton.setAvm(skeleton.get(0).getAvm());
		for(int i = 0; i < skeleton.getSize(); i++)
		{
			try {
				skeleton.setAvm(skeleton.getAvm().unify(skeleton.get(i).getAvm()));
			} catch (FeatureTypesDontMatchException | InvalidFeatureValueTypeException e) {
				e.printStackTrace();
			}
		}
		return skeleton;
	}
	
	/**
	 * Generates a list of words that are chosen from a skeleton grammar that contains
	 * 					only word lists. Used a bit string to select the words
	 * @param skeleton		The skeleton grammar
	 * @param iterator		A Bit String Iterator that returns integers
	 * @return				LinkedList of the chosen words
	 */
	private LinkedList<Word> fillGrammar(Clause skeleton, BitStringIterator iterator)
	{
		LinkedList<Word> words = new LinkedList<Word>();
		for(int j = 0; j < skeleton.getSize(); j++)
		{
			WordList item = skeleton.get(j).getLexicon();
			int b = iterator.next() % item.getSize(); 
			words.add(item.getWord(b));
		}
		
		return words;
	}
	
	/**
	 * Method to select words from a list of end-node-only nodes in a clause
	 * @param words			A list of word objects to select inflections from
	 * @param avm			The attribute value matrix for the list of words
	 * @param iterator		The bitstring iterator
	 * @return				A list of words with proper inflections
	 */
	private LinkedList<String> clause2wordList(LinkedList<Word> words, AttributeValueMatrix avm, BitStringIterator iterator)
	{
		LinkedList<String> result = new LinkedList<String>();
		for(int i = 0; i < words.size(); i++)
		{
			ArrayList<String> allowedWords = words.get(i).getWordsByAvm(avm);
			int b = iterator.next() % allowedWords.size();
			result.add(allowedWords.get(b));
		}
		
		return result;
	}
}
