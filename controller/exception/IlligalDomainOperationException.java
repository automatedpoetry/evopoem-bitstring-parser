package controller.exception;

public class IlligalDomainOperationException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IlligalDomainOperationException()
	{
		super("Illegal domain operation exception: can't alter the domain of an attribute value matrix. Alter features instead");
	}
}
