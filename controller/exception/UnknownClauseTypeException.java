package controller.exception;

public class UnknownClauseTypeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public UnknownClauseTypeException()
	{
		super();
	}
}
