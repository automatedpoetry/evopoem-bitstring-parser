package controller.exception;

public class ValueNotInstantiatedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ValueNotInstantiatedException()
	{
		super("Domain of feature contains multiple values. Use constraint satisfaction to fix the domain before requesting a value");
	}

}
