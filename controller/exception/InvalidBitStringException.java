package controller.exception;

public class InvalidBitStringException extends Exception {
	private static final long serialVersionUID = -8236310083595567653L;
	
	public InvalidBitStringException(String e)
	{
		super(e);
	}
}