package controller.exception;

public class InvalidFeatureValueTypeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public InvalidFeatureValueTypeException()
	{
		super("Invalid feature types in arguments");
	}
	
	public InvalidFeatureValueTypeException(String e)
	{
		super(e);
	}

}
