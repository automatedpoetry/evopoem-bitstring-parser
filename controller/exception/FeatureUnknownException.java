package controller.exception;

public class FeatureUnknownException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public FeatureUnknownException(String name)
	{
		super(String.format("Feature name or path '%s' is unknown in the Attribute Value Matrix. Please check this string for errors", name));
	}

}
