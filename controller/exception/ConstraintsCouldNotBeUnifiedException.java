package controller.exception;

public class ConstraintsCouldNotBeUnifiedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Throw a new unification exception, displaying the element unification failed on
	 * @param feature	The name of the feature that unification failed on
	 */
	public ConstraintsCouldNotBeUnifiedException(String feature)
	{
		super(String.format("Constraint matrixes could not be unified on %s", feature));
	}

}
