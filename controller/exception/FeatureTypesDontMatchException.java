package controller.exception;

import model.grammar.feature.Feature;

public class FeatureTypesDontMatchException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FeatureTypesDontMatchException(int f1, int f2)
	{
		super(String.format("Failed at matching feature of type %s with feature of type %s", 
				Feature.getFeatureNameForFeatureType(f1), Feature.getFeatureNameForFeatureType(f2)
			));
	}
	
}
