package controller.exception;

public class UnificationFailureOnFeaturesException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public UnificationFailureOnFeaturesException()
	{
		super("Features could not be unified. Domains do not match");
	}

}
