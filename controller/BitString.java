package controller;

import java.util.Iterator;

import controller.exception.InvalidBitStringException;

public class BitString implements Iterable<Integer> {
	private static final Integer DEFAULT_MIN_LENGTH = 10;
	private static final Integer DEFAULT_MAX_LENGTH = 30;
	private String bits;
	private int unitLength;
	
	/**
	 * Construct bit string from string of bits with a default unit length of 4.
	 * @param bits	String of bits, may only contain 1's and zero's
	 * @throws InvalidBitStringException
	 * 			If any other character than 1 or 0 is included in the
	 * 			bit string
	 */
	public BitString(String bits) throws InvalidBitStringException
	{
		this(bits, 4);
	}
	
	/**
	 * Construct bit string from string of bits
	 * @param bits	String of bits, may only contain 1's and zero's
	 * @param unitLength Amount of bits per unit.
	 * @throws InvalidBitStringException
	 * 			If any other character than 1 or 0 is included in the
	 * 			bit string
	 */
	public BitString(String bits, int unitLength) throws InvalidBitStringException
	{
		if (isValid(bits))
		{
			this.bits = bits;
			this.unitLength = unitLength;
		}
	}
	
	/**
	 * Method checks if bit string contains no other character than
	 * 			one's and zero's
	 * @param bits	string of bits
	 * @return		True if valid bit string
	 * @throws InvalidBitStringException
	 * 			If any other character than 1 or 0 is included in the
	 * 			bit string
	 */
	private boolean isValid(String bits) throws InvalidBitStringException
	{
		for(int i = 0; i < bits.length(); i++)
		{
			if(bits.charAt(i) != '1' && bits.charAt(i) != '0') 
				throw new InvalidBitStringException(
						String.format(
								"Invalid character '%s' at index %d", 
								bits.charAt(i), i
							)
					);
		}
		return bits.length() > 0;
	}
	
	/**
	 * Copies current bit string into new bit string
	 * @return	bit string
	 * @throws InvalidBitStringException
	 * 			If any other character than 1 or 0 is included in the
	 * 			bit string
	 */
	public BitString copy() throws InvalidBitStringException
	{
		return new BitString(bits, this.unitLength);
	}
	
	/**
	 * Get method for bitstring
	 * @return	string of bits
	 */
	public String getBits()
	{
		return bits;
	}
	
	/**
	 * Returns a random bit string
	 * @return	Random bit string
	 */
	public static BitString random()
	{
		return BitString.random(DEFAULT_MIN_LENGTH, DEFAULT_MAX_LENGTH);
	}
	
	/**
	 * Returns a random bit string of length length
	 * @param length	The length of the random bit string
	 * @return			Random bit string of length length
	 */
	public static BitString random(int length)
	{
		String bitstring = "";
		for(int i = 0; i < length; i++)
		{
			bitstring += (Math.random() > 0.5) ? "0" : "1"; 
		}
		try {
			return new BitString(bitstring);
		} catch (InvalidBitStringException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	
	/**
	 * Returns a random bit string of a length between
	 * 				min and max
	 * @param min	Minimal bit string length
	 * @param max	Maximum bit string length
	 * @return		Random bit string
	 */
	public static BitString random(int min, int max)
	{
		return BitString.random(min + (int)(Math.random() * ((max - min) + 1)));
	}
	
	/**
	 * Method to mutate the bitstring
	 * @param target	Bitstring to mutate
	 * @return			Mutated bitstring
	 */
	public static BitString mutate(BitString target)
	{
		String oldBits = target.getBits();
		String newBits = "";
		// TODO: Think about mutation rate: 
		float mutationRate = 1.0f / (target.getBits().length() * 5);
		for (int i = 0; i < target.getBits().length(); i++)
		{
			String c = Character.toString(oldBits.charAt(i));
			newBits += (Math.random() < mutationRate ? ((c == "0") ? "1" : "0") : c);
		}
		try
		{
			return new BitString(newBits);
		}
		catch (InvalidBitStringException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Method to perform crossover on a series of bitstring
	 * UNIMPLEMENTED
	 * @param companions	Bitstrings to perform crossover on
	 * @return				List of crossed over bitstrings
	 */
	public static BitString[] crossover(BitString... companions)
	{
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Get the iterator for the current bit string
	 * Iterator returns integers on Iterator.next()
	 */
	public Iterator<Integer> iterator()
	{
		return new BitStringIterator(bits, unitLength);
	}
	
	/**
	 * ToString method
	 */
	public String toString()
	{
		return String.format("<BitString(bits='%s')>", bits);
	}
}
