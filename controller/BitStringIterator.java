package controller;

import java.util.Iterator;

/**
 * Iterator object for bitstring class
 */
public class BitStringIterator implements Iterator<Integer> {
	private int index = 0;
	private String source;
	private int unitLength;
	
	/**
	 * Constructor
	 * @param bits			String of bits
	 * @param unitLength	Length of one unit that is converted to integer
	 */
	public BitStringIterator(String bits, int unitLength)
	{
		this.source = bits;
		this.unitLength = unitLength;
	}
	
	/**
	 * Has next method. Always returns true, as the iterator
	 * starts over from the beginning when the end of a string
	 * is reached
	 */
	public boolean hasNext()
	{
		return true;
	}
	
	/**
	 * Method to return next bitstring integer
	 */
	public Integer next()
	{
		String subStr = "";
		while (subStr.length() < unitLength)
		{
			if (index >= source.length())
				index = 0;
			subStr += source.substring(index, index + 1);
			index++;
		}
		return Integer.parseInt(subStr, 2);
	}
}
