package model.word;

/**
 * Tuple of a String and an int. The int can refer to the inflection type and
 * the string is the word
 * 
 * @author A.J de Mooij
 */
public class Inflection {

	/**
	 * Reference to the inflection
	 */
	private String word;

	/**
	 * Reference to the inflection type
	 */
	private int type;

	/**
	 * Amount of syllables in pronunciation of the word
	 */
	private int syllables;

	/**
	 * The syllable that has the emphasis
	 */
	private int emphasis;

	/**
	 * Constructor for an inflection
	 * 
	 * @param word
	 *            The inflection of a word
	 * @param type
	 *            The type of the inflection
	 * @param syllables
	 *            The number of syllables in pronunciation of the inflection
	 * @param emphasis
	 *            Which syllable has the emphasis
	 */
	public Inflection(String word, int type, int syllables, int emphasis) {
		this.word = word;
		this.type = type;
		this.syllables = syllables;
		this.emphasis = emphasis;
	}

	/**
	 * Method to get the inflection of the word
	 * 
	 * @return The inflection of the word
	 */
	public String getWord() {
		return this.word;
	}

	/**
	 * Method to get the type of this inflection
	 * 
	 * @return The type of this inflection
	 */
	public int getType() {
		return this.type;
	}

	/**
	 * Get the number of syllables in the inflection
	 * 
	 * @return Number of syllables
	 */
	public int getSyllables() {
		return this.syllables;
	}

	/**
	 * Get the the number of the syllable that has the emphasis
	 * 
	 * @return Number of the syllable that has emphasis
	 */
	public int getEmphasis() {
		return this.emphasis;
	}
}
