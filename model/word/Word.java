package model.word;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

import controller.exception.DuplicateWordTypeException;
import controller.exception.FeatureUnknownException;
import controller.exception.InvalidFeatureValueTypeException;
import controller.exception.NoWordForInflectionException;
import controller.exception.ValueNotInstantiatedException;
import model.grammar.feature.AttributeValueMatrix;

public class Word {

	/**
	 * Word types
	 */
	public static int DT_BEFORE_CONSONANT = 0;
	public static int DT_BEFORE_VOWEL = 1;
	public static int DT_BEFORE_BOTH = 2;

	/**
	 * Noun types
	 */
	public static int NOUN_SINGULAR = 10;
	public static int NOUN_PLURAL = 11;
	
	/**
	 * Pronoun types
	 */
	public static int PRONOUN_PERSONAL = 20;
	public static int PRONOUN_DISTANT = 21;
	public static int PRONOUN_POSSESIVE = 22;
	public static int PRONOUN_POSSESIVE_DISTANT = 23;
	public static int PRONOUN_PLURAL_PERSONAL = 24;
	public static int PRONOUN_PLURAL_DISTANT = 25;
	public static int PRONOUN_PLURAL_POSSESIVE = 26;
	public static int PRONOUN_PLURAL_POSSESIVE_DISTANT = 27;

	/**
	 * Verb types
	 */
	public static int VERB_BASE_FORM = 30;
	public static int VERB_PAST_TENSE = 31;
	public static int VERB_GERUND = 32;
	public static int VERB_PAST_PARTICIPLE = 33;
	public static int VERB_NON_3GS_PRESENT = 34;
	public static int VERB_3GS_PRESENT = 35;

	/**
	 * Adverb types
	 */
	public static int ADVERB = 40;
	public static int ADVERB_COMPARATIVE = 41;
	public static int ADVERB_SUPERLATIVE = 42;
	
	public static int ADJ = 50;
	public static int ADJ_COMP = 51;
	public static int ADJ_SUP = 52;

	protected String word;
	protected LinkedList<Inflection> inflections;
	
	/**
	 * Get all possible inflections for this word based on the AttributeValueMatrix
	 * @param avm
	 * @return
	 */
	public ArrayList<String> getWordsByAvm(AttributeValueMatrix avm)
	{
		ArrayList<Integer> allowedTypes = new ArrayList<Integer>(
				Arrays.asList(ADVERB, ADVERB_COMPARATIVE, ADVERB_SUPERLATIVE,
				ADJ, ADJ_COMP, ADJ_SUP,
				DT_BEFORE_CONSONANT, DT_BEFORE_VOWEL, DT_BEFORE_BOTH,
				NOUN_PLURAL, NOUN_SINGULAR,
				PRONOUN_PERSONAL, PRONOUN_PLURAL_PERSONAL, 
				VERB_3GS_PRESENT, VERB_NON_3GS_PRESENT, VERB_PAST_PARTICIPLE, VERB_PAST_TENSE
			));
		try {
			Object p = avm.getFeature("agreement.person").getValue();
			if(p.equals(1))
			{
				allowedTypes.removeAll(Arrays.asList( 
							VERB_3GS_PRESENT 
						));
			} else if(p.equals(2)) {
				allowedTypes.removeAll(Arrays.asList(
						VERB_3GS_PRESENT
					));
			} else if (p.equals(3)) {
				allowedTypes.removeAll(Arrays.asList(
							VERB_3GS_PRESENT
						));
			}
		} catch (ValueNotInstantiatedException | InvalidFeatureValueTypeException | FeatureUnknownException e) {
			e.printStackTrace();
		}
		try{
			Object n = avm.getFeature("agreement.number").getValue();
			if(n.equals("sg")){
				allowedTypes.removeAll(Arrays.asList(
							NOUN_PLURAL
						));
			} else if (n.equals("pl"))
			{
				allowedTypes.removeAll(Arrays.asList(
						DT_BEFORE_CONSONANT, DT_BEFORE_VOWEL,
						NOUN_SINGULAR, PRONOUN_PERSONAL,
						VERB_3GS_PRESENT
						));
			}
		} catch(ValueNotInstantiatedException | InvalidFeatureValueTypeException | FeatureUnknownException e) {
			e.printStackTrace();
		}
		
		ArrayList<String> wordsResult = new ArrayList<String>();
		
		
		for(Inflection i : inflections)
		{
			if(allowedTypes.contains(i.getType()))
			{
				wordsResult.add(i.getWord());
			}
		}
		
		if(wordsResult.isEmpty())
		{
			inflections.forEach(i -> wordsResult.add(i.getWord()));
		}
		
		return wordsResult;
	}

	
	public Word(String word, LinkedList<Inflection> inflections)
	{
		this.word = word;
		this.inflections = inflections;
	}
	
	/**
	 * Get method for the word
	 * @return The word
	 */
	public String getWord() {
		return this.word;
	}
	
	public void addInflection(Inflection inflection) throws DuplicateWordTypeException
	{
		int t = inflection.getType();
		for(Inflection i : this.inflections)
		{
			if (i.getType() == t)
			{
				throw new DuplicateWordTypeException();
			}
		}
		this.inflections.add(inflection);
	}

	/**
	 * Get the inflection for a word
	 * 
	 * @param inflection
	 *            The type of inflection
	 * @return The inflection of the word
	 * @throws NoWordForInflectionException 
	 */
	public String getInflection(int before_type) throws NoWordForInflectionException{
		for (Inflection i : inflections) {
			if (i.getType() == before_type)
				return i.getWord();
		}
		throw new NoWordForInflectionException();
	}
	
	
}
