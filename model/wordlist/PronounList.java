package model.wordlist;

import java.util.LinkedList;
import model.word.*;

public class PronounList extends WordList {

	public PronounList()
	{
		LinkedList<Inflection> i = new LinkedList<Inflection>();
		i.add(new Inflection("I", Word.PRONOUN_PERSONAL, 1, 0));
		i.add(new Inflection("me", Word.PRONOUN_DISTANT, 1, 0));
		i.add(new Inflection("my", Word.PRONOUN_POSSESIVE, 1,0));
		i.add(new Inflection("mine", Word.PRONOUN_POSSESIVE_DISTANT, 1, 0));
		i.add(new Inflection("we", Word.PRONOUN_PLURAL_PERSONAL, 1, 0));
		i.add(new Inflection("us", Word.PRONOUN_PLURAL_DISTANT, 1, 0));
		i.add(new Inflection("our", Word.PRONOUN_PLURAL_POSSESIVE, 1,0));
		i.add(new Inflection("ours", Word.PRONOUN_PLURAL_POSSESIVE_DISTANT, 1, 0));
		
		LinkedList<Inflection> you = new LinkedList<Inflection>();
		you.add(new Inflection("you", Word.PRONOUN_PERSONAL, 1, 0));
		you.add(new Inflection("you", Word.PRONOUN_DISTANT, 1, 0));
		you.add(new Inflection("yours", Word.PRONOUN_POSSESIVE, 1,0));
		you.add(new Inflection("yours", Word.PRONOUN_POSSESIVE_DISTANT, 1, 0));
		you.add(new Inflection("you", Word.PRONOUN_PLURAL_PERSONAL, 1, 0));
		you.add(new Inflection("you", Word.PRONOUN_PLURAL_DISTANT, 1, 0));
		you.add(new Inflection("yours", Word.PRONOUN_PLURAL_POSSESIVE, 1,0));
		you.add(new Inflection("yours", Word.PRONOUN_PLURAL_POSSESIVE_DISTANT, 1, 0));
		
		LinkedList<Inflection> he = new LinkedList<Inflection>();
		he.add(new Inflection("he", Word.PRONOUN_PERSONAL, 1, 0));
		he.add(new Inflection("him", Word.PRONOUN_DISTANT, 1, 0));
		he.add(new Inflection("his", Word.PRONOUN_POSSESIVE, 1,0));
		he.add(new Inflection("his", Word.PRONOUN_POSSESIVE_DISTANT, 1, 0));
		he.add(new Inflection("they", Word.PRONOUN_PLURAL_PERSONAL, 1, 0));
		he.add(new Inflection("them", Word.PRONOUN_PLURAL_DISTANT, 1, 0));
		he.add(new Inflection("their", Word.PRONOUN_PLURAL_POSSESIVE, 1,0));
		he.add(new Inflection("theirs", Word.PRONOUN_PLURAL_POSSESIVE_DISTANT, 1, 0));
		
		LinkedList<Inflection> she = new LinkedList<Inflection>();
		she.add(new Inflection("she", Word.PRONOUN_PERSONAL, 1, 0));
		she.add(new Inflection("her", Word.PRONOUN_DISTANT, 1, 0));
		she.add(new Inflection("her", Word.PRONOUN_POSSESIVE, 1,0));
		she.add(new Inflection("hers", Word.PRONOUN_POSSESIVE_DISTANT, 1, 0));
		she.add(new Inflection("they", Word.PRONOUN_PLURAL_PERSONAL, 1, 0));
		she.add(new Inflection("them", Word.PRONOUN_PLURAL_DISTANT, 1, 0));
		she.add(new Inflection("their", Word.PRONOUN_PLURAL_POSSESIVE, 1,0));
		she.add(new Inflection("theirs", Word.PRONOUN_PLURAL_POSSESIVE_DISTANT, 1, 0));
		
		words.add(new Word("I", i));
		words.add(new Word("you", you));
		words.add(new Word("he", he));
		words.add(new Word("she", she));

		createFeatureMap();
	}

	protected void createFeatureMap() {
		this.avm = this.getAgreeAllAvm();
	}

}
