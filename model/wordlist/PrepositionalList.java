package model.wordlist;

import java.util.Arrays;
import java.util.LinkedList;

import model.grammar.feature.AttributeValueMatrix;
import model.word.Inflection;
import model.word.Word;

public class PrepositionalList extends WordList {
	
	public PrepositionalList()
	{
		words.add(new Word("from", new LinkedList<Inflection>(Arrays.asList(new Inflection("from", 0, 1, 1)))));
		words.add(new Word("to", new LinkedList<Inflection>(Arrays.asList(new Inflection("to", 0, 1, 1)))));
		words.add(new Word("on", new LinkedList<Inflection>(Arrays.asList(new Inflection("on", 0, 1, 1)))));
		words.add(new Word("through", new LinkedList<Inflection>(Arrays.asList(new Inflection("through", 0, 1, 1)))));
		words.add(new Word("near", new LinkedList<Inflection>(Arrays.asList(new Inflection("near", 0, 1, 1)))));

		createFeatureMap();
	}

	protected void createFeatureMap() {
		this.avm = new AttributeValueMatrix();
	}
	
	
}
