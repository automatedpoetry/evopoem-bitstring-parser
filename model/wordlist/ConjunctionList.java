package model.wordlist;

import java.util.LinkedList;

import model.word.Word;
import model.grammar.feature.AttributeValueMatrix;
import model.word.Inflection;

public class ConjunctionList extends WordList {
	
	public ConjunctionList() {
		LinkedList<Inflection> f = new LinkedList<Inflection>();
		f.add(new Inflection("for", 0, 1,0));
		
		LinkedList<Inflection> a = new LinkedList<Inflection>();
		a.add(new Inflection("and", 0, 1, 0));
		
		LinkedList<Inflection> n = new LinkedList<Inflection>();
		n.add(new Inflection("nor", 0, 1, 0));
		
		LinkedList<Inflection> b = new LinkedList<Inflection>();
		b.add(new Inflection("but", 0, 1, 0));
		
		LinkedList<Inflection> o = new LinkedList<Inflection>();
		o.add(new Inflection("or", 0, 1, 0));
		
		LinkedList<Inflection> y = new LinkedList<Inflection>();
		y.add(new Inflection("yet", 0, 1, 0));
		
		LinkedList<Inflection> s = new LinkedList<Inflection>();
		s.add(new Inflection("so", 0, 1, 0));
		
		words.add(new Word("for", f));
		words.add(new Word("and", a));
		words.add(new Word("nor", n));
		words.add(new Word("but", b));
		words.add(new Word("or", o));
		words.add(new Word("yet", y));
		words.add(new Word("so", s));

		createFeatureMap();
	}

	protected void createFeatureMap() {
		this.avm = new AttributeValueMatrix();
	}
}
