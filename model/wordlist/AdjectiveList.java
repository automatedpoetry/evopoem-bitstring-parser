package model.wordlist;

import java.util.LinkedList;

import model.grammar.feature.AttributeValueMatrix;
import model.word.Inflection;
import model.word.Word;

public class AdjectiveList extends WordList {
	
	public AdjectiveList()
	{
		LinkedList<Inflection> yellow = new LinkedList<Inflection>();
		yellow.add(new Inflection("yellow", Word.ADJ, 2, 0));
		yellow.add(new Inflection("yellower", Word.ADJ_COMP, 3, 0));
		yellow.add(new Inflection("yellowest", Word.ADJ_SUP, 3, 0));
		
		LinkedList<Inflection> big = new LinkedList<Inflection>();
		big.add(new Inflection("big", Word.ADJ, 1, 0));
		big.add(new Inflection("bigger", Word.ADJ_COMP, 2, 0));
		big.add(new Inflection("biggest", Word.ADJ_SUP, 2, 0));
		
		LinkedList<Inflection> large = new LinkedList<Inflection>();
		large.add(new Inflection("large", Word.ADJ, 1, 0));
		large.add(new Inflection("larger", Word.ADJ_COMP, 2, 0));
		large.add(new Inflection("largest", Word.ADJ_SUP, 2, 0));
		
		LinkedList<Inflection> small = new LinkedList<Inflection>();
		small.add(new Inflection("small", Word.ADJ, 1, 0));
		small.add(new Inflection("smaller", Word.ADJ_COMP, 2, 0));
		small.add(new Inflection("smallest", Word.ADJ_SUP, 2, 0));
		
		LinkedList<Inflection> hopeless = new LinkedList<Inflection>();
		hopeless.add(new Inflection("hopeless", Word.ADJ, 2, 0));
		hopeless.add(new Inflection("more hopeless", Word.ADJ_COMP, 3, 1));
		hopeless.add(new Inflection("most hopeless", Word.ADJ_SUP, 3, 1));
		
		LinkedList<Inflection> grotesque = new LinkedList<Inflection>();
		grotesque.add(new Inflection("grotesque", Word.ADJ, 2, 1));
		grotesque.add(new Inflection("more grotesque", Word.ADJ_COMP, 3, 2));
		grotesque.add(new Inflection("most grotesque", Word.ADJ_SUP, 3, 2));
		
		LinkedList<Inflection> ghastly = new LinkedList<Inflection>();
		ghastly.add(new Inflection("ghastly", Word.ADJ, 2, 0));
		ghastly.add(new Inflection("more ghastly", Word.ADJ_COMP, 3, 1));
		ghastly.add(new Inflection("most ghastly", Word.ADJ_SUP, 3, 1));
		
		LinkedList<Inflection> beautiful = new LinkedList<Inflection>();
		beautiful.add(new Inflection("beautiful", Word.ADJ, 3, 0));
		beautiful.add(new Inflection("more beautiful", Word.ADJ_COMP, 4, 1));
		beautiful.add(new Inflection("most beautiful", Word.ADJ_SUP, 4, 1));
		
		LinkedList<Inflection> abominable = new LinkedList<Inflection>();
		abominable.add(new Inflection("abominable", Word.ADJ, 5, 1));
		abominable.add(new Inflection("more abominable", Word.ADJ_COMP, 6, 2));
		abominable.add(new Inflection("most abominable", Word.ADJ_SUP, 6, 2));
		
		LinkedList<Inflection> good = new LinkedList<Inflection>();
		good.add(new Inflection("good", Word.ADJ, 1, 0));
		good.add(new Inflection("better", Word.ADJ_COMP, 2, 0));
		good.add(new Inflection("best", Word.ADJ_SUP, 1, 0));
		
		LinkedList<Inflection> bad = new LinkedList<Inflection>();
		bad.add(new Inflection("bad", Word.ADJ, 1, 0));
		bad.add(new Inflection("worse", Word.ADJ_COMP, 1, 0));
		bad.add(new Inflection("worst", Word.ADJ_SUP, 1, 0));
		
		LinkedList<Inflection> brilliant = new LinkedList<Inflection>();
		brilliant.add(new Inflection("brilliant", Word.ADJ, 3, 0));
		brilliant.add(new Inflection("more brilliant", Word.ADJ_COMP, 4, 1));
		brilliant.add(new Inflection("most brilliant", Word.ADJ_SUP, 4, 1));
		
		LinkedList<Inflection> amazing = new LinkedList<Inflection>();
		amazing.add(new Inflection("amazing", Word.ADJ, 3, 1));
		amazing.add(new Inflection("more amazing", Word.ADJ_COMP, 4, 2));
		amazing.add(new Inflection("most amazing", Word.ADJ_SUP, 4, 2));
		
		LinkedList<Inflection> magnificant = new LinkedList<Inflection>();
		magnificant.add(new Inflection("magnificant", Word.ADJ, 4, 1));
		magnificant.add(new Inflection("more magnificant", Word.ADJ_COMP, 5, 2));
		magnificant.add(new Inflection("most magnificant", Word.ADJ_SUP, 5, 2));
		
		words.add(new Word("yellow", yellow));
		words.add(new Word("big", big));
		words.add(new Word("large", large));
		words.add(new Word("small", small));
		words.add(new Word("hopeless", hopeless));
		words.add(new Word("grotesque", grotesque));
		words.add(new Word("ghastly", ghastly));
		words.add(new Word("beautiful", beautiful));
		words.add(new Word("abominable", abominable));
		words.add(new Word("good", good));
		words.add(new Word("bad", bad));
		words.add(new Word("brilliant", brilliant));
		words.add(new Word("amazing", amazing));
		words.add(new Word("magnificant", magnificant));
		
		createFeatureMap();
		
	}
	
	protected void createFeatureMap()
	{
		this.avm = new AttributeValueMatrix();
	}

}
