package model.wordlist;

import java.util.LinkedList;

import model.grammar.feature.AttributeValueMatrix;
import model.word.Inflection;
import model.word.Word;

public class AdverbList extends WordList {
	
	public AdverbList()
	{
		LinkedList<Inflection> slowly = new LinkedList<Inflection>();
		slowly.add(new Inflection("slowly", Word.ADVERB, 2, 0));
		slowly.add(new Inflection("slower", Word.ADVERB_COMPARATIVE, 2, 0));
		slowly.add(new Inflection("slowest", Word.ADVERB_SUPERLATIVE, 2, 0));
		
		LinkedList<Inflection> fast = new LinkedList<Inflection>();
		fast.add(new Inflection("fast", Word.ADVERB, 1, 0));
		fast.add(new Inflection("faster", Word.ADVERB_COMPARATIVE, 2, 0));
		fast.add(new Inflection("fastest", Word.ADVERB_SUPERLATIVE, 2, 0));
		
		LinkedList<Inflection> quickly = new LinkedList<Inflection>();
		quickly.add(new Inflection("quickly", Word.ADVERB, 2, 0));
		quickly.add(new Inflection("quicklier", Word.ADVERB_COMPARATIVE, 3, 0));
		quickly.add(new Inflection("quickliest", Word.ADVERB_SUPERLATIVE, 3, 0));
		
		LinkedList<Inflection> abruptly = new LinkedList<Inflection>();
		abruptly.add(new Inflection("abruptly", Word.ADVERB, 3, 1));
		abruptly.add(new Inflection("more abruptly", Word.ADVERB_COMPARATIVE, 4, 2));
		abruptly.add(new Inflection("more abruptly", Word.ADVERB_SUPERLATIVE, 4, 2));
		
		LinkedList<Inflection> lightly = new LinkedList<Inflection>();
		lightly.add(new Inflection("lightly", Word.ADVERB, 2, 0));
		lightly.add(new Inflection("lightlier", Word.ADVERB_COMPARATIVE, 3, 1));
		lightly.add(new Inflection("lightliest", Word.ADVERB_SUPERLATIVE, 3, 1));
		
		LinkedList<Inflection> wearily = new LinkedList<Inflection>();
		wearily.add(new Inflection("wearily", Word.ADVERB, 3, 0));
		wearily.add(new Inflection("more wearily", Word.ADVERB_COMPARATIVE, 4, 1));
		wearily.add(new Inflection("most wearily", Word.ADVERB_SUPERLATIVE, 4, 1));
		
		LinkedList<Inflection> well = new LinkedList<Inflection>();
		well.add(new Inflection("well", Word.ADVERB, 1, 0));
		well.add(new Inflection("better", Word.ADVERB_COMPARATIVE, 2, 0));
		well.add(new Inflection("best", Word.ADVERB_SUPERLATIVE, 2, 0));
		
		LinkedList<Inflection> easily = new LinkedList<Inflection>();
		easily.add(new Inflection("easily", Word.ADVERB, 3, 0));
		easily.add(new Inflection("more easily", Word.ADVERB_COMPARATIVE, 4, 1));
		easily.add(new Inflection("easiliest", Word.ADVERB_SUPERLATIVE, 4, 0));
		
		LinkedList<Inflection> uneasily = new LinkedList<Inflection>();
		uneasily.add(new Inflection("uneasily", Word.ADVERB, 4, 1));
		uneasily.add(new Inflection("less easily", Word.ADVERB_COMPARATIVE, 4, 1));
		uneasily.add(new Inflection("least easily", Word.ADVERB_SUPERLATIVE, 4, 1));
		
		LinkedList<Inflection> weirdly = new LinkedList<Inflection>();
		weirdly.add(new Inflection("weirdly", Word.ADVERB, 2, 0));
		weirdly.add(new Inflection("weirdlier", Word.ADVERB_COMPARATIVE, 3, 0));
		weirdly.add(new Inflection("weirdliest", Word.ADVERB_SUPERLATIVE, 3, 0));
		
		LinkedList<Inflection> expertly = new LinkedList<Inflection>();
		expertly.add(new Inflection("expertly", Word.ADVERB, 3, 0));
		expertly.add(new Inflection("expertlier", Word.ADVERB_COMPARATIVE, 4, 0));
		expertly.add(new Inflection("expertliest", Word.ADVERB_SUPERLATIVE, 4, 0));
		
		LinkedList<Inflection> cheerfully = new LinkedList<Inflection>();
		cheerfully.add(new Inflection("cheerfully", Word.ADVERB, 3, 0));
		cheerfully.add(new Inflection("cheerfullier", Word.ADVERB_COMPARATIVE, 4, 0));
		cheerfully.add(new Inflection("cheerfulliest", Word.ADVERB_SUPERLATIVE, 4, 0));
		
		LinkedList<Inflection> wholeheartedly = new LinkedList<Inflection>();
		wholeheartedly.add(new Inflection("wholeheartedly", Word.ADVERB, 4, 1));
		wholeheartedly.add(new Inflection("wholeheartedlier", Word.ADVERB_COMPARATIVE, 5, 1));
		wholeheartedly.add(new Inflection("wholeheartedliest", Word.ADVERB_SUPERLATIVE, 5, 1));
		
		LinkedList<Inflection> randomly = new LinkedList<Inflection>();
		randomly.add(new Inflection("randomly", Word.ADVERB, 3, 0));
		randomly.add(new Inflection("randomlier", Word.ADVERB_COMPARATIVE, 4, 0));
		randomly.add(new Inflection("randomliest", Word.ADVERB_SUPERLATIVE, 4, 0));
		
		words.add(new Word("slowly", slowly));
		words.add(new Word("fast", fast));
		words.add(new Word("quickly", quickly));
		words.add(new Word("abruptly", abruptly));
		words.add(new Word("lightly", lightly));
		words.add(new Word("wearily", wearily));
		words.add(new Word("well", well));
		words.add(new Word("easily", easily));
		words.add(new Word("uneasily", uneasily));
		words.add(new Word("weirdly", weirdly));
		words.add(new Word("expertly", expertly));
		words.add(new Word("cheerfully", cheerfully));
		words.add(new Word("wholeheartedly", wholeheartedly));
		words.add(new Word("randomly", randomly));
		

		createFeatureMap();
	}

	protected void createFeatureMap() {
		this.avm = new AttributeValueMatrix();
	}

}
