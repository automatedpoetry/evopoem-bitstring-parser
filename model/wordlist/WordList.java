package model.wordlist;

import java.util.LinkedList;

import model.grammar.feature.AttributeValueMatrix;
import model.grammar.feature.Feature;
import model.word.Word;

/**
 * Each type in the grammar gets its own word list The words are saved in a
 * LinkedList and each have their own class to make sure extra attributes can be
 * added later easily
 */
public abstract class WordList {
	
	protected LinkedList<Word> words = new LinkedList<Word>();
	protected AttributeValueMatrix avm;
	
	/**
	 * Returns the word at the given index
	 * 
	 * @param index
	 *            The index of the word
	 * @return The requested word
	 */
	public Word getWord(int index) throws IndexOutOfBoundsException
	{
		if(index < 0 || index > words.size())
		{
			throw new IndexOutOfBoundsException(
					String.format(
							"Word of index %i could not be found in list of length %i", 
							index, words.size()
						)
					);
		} else {
			return words.get(index);
		}
	}
	
	/**
	 * Returns the number of words in this list
	 * @return The number of words in this list
	 */
	public int getSize()
	{
		return words.size();
	}
	
	public String toString()
	{
		String className = this.getClass().getSimpleName().toUpperCase();
		if (className.endsWith("LIST"))
		{
			className = className.substring(0, className.length() - 4);
			System.out.println(className);
		}
		return className;
	}
	
	protected AttributeValueMatrix getAgreeAllAvm()
	{
		Feature fPerson = new Feature("person", 1,2,3);
		Feature fNumber = new Feature("number", "sg", "pl");
		AttributeValueMatrix agreePersonNumber = new AttributeValueMatrix();
		agreePersonNumber.addFeature(fPerson);
		agreePersonNumber.addFeature(fNumber);
		Feature agreement = new Feature("agreement", agreePersonNumber);
		AttributeValueMatrix agreeAll = new AttributeValueMatrix();
		agreeAll.addFeature(agreement);
		return agreeAll;
	}
	
	/**
	 * Returns the word lists default AVM domain
	 * @return	World lists default AVM domain
	 */
	public AttributeValueMatrix getAvm()
	{
		return this.avm;
	}
	
	/**
	 * Method to add a default avm to a word list
	 */
	protected abstract void createFeatureMap();
}
