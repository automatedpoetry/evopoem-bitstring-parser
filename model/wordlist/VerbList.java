package model.wordlist;

import java.util.LinkedList;

import model.word.Inflection;
import model.word.Word;

public class VerbList extends WordList {
	
	public VerbList()
	{
		LinkedList<Inflection> ache = new LinkedList<Inflection>();
		ache.add(new Inflection("ache", Word.VERB_BASE_FORM, 1, 0));
		ache.add(new Inflection("ached", Word.VERB_PAST_TENSE, 2, 0));
		ache.add(new Inflection("aching", Word.VERB_GERUND, 2, 0));
		ache.add(new Inflection("ached", Word.VERB_PAST_PARTICIPLE, 2, 0));
		ache.add(new Inflection("ache", Word.VERB_NON_3GS_PRESENT, 1, 0));
		ache.add(new Inflection("aches", Word.VERB_3GS_PRESENT, 1, 0));
		
		LinkedList<Inflection> alert = new LinkedList<Inflection>();
		alert.add(new Inflection("alert", Word.VERB_BASE_FORM, 2, 1));
		alert.add(new Inflection("alerted", Word.VERB_PAST_TENSE, 3, 1));
		alert.add(new Inflection("alerting", Word.VERB_GERUND, 3, 1));
		alert.add(new Inflection("alerted", Word.VERB_PAST_PARTICIPLE, 3, 1));
		alert.add(new Inflection("alert", Word.VERB_NON_3GS_PRESENT, 2, 1));
		alert.add(new Inflection("alerts", Word.VERB_3GS_PRESENT, 2, 1));
		
		LinkedList<Inflection> answer = new LinkedList<Inflection>();
		answer.add(new Inflection("answer", Word.VERB_BASE_FORM, 2, 0));
		answer.add(new Inflection("answered", Word.VERB_PAST_TENSE, 2, 0));
		answer.add(new Inflection("answering", Word.VERB_GERUND, 3, 0));
		answer.add(new Inflection("answered", Word.VERB_PAST_PARTICIPLE, 2, 0));
		answer.add(new Inflection("answer", Word.VERB_NON_3GS_PRESENT, 2, 0));
		answer.add(new Inflection("answers", Word.VERB_3GS_PRESENT, 2, 0));
		
		LinkedList<Inflection> act = new LinkedList<Inflection>();
		act.add(new Inflection("act", Word.VERB_BASE_FORM, 1, 0));
		act.add(new Inflection("acted", Word.VERB_PAST_TENSE, 2, 0));
		act.add(new Inflection("acting", Word.VERB_GERUND, 2, 0));
		act.add(new Inflection("acted", Word.VERB_PAST_PARTICIPLE, 2, 0));
		act.add(new Inflection("act", Word.VERB_NON_3GS_PRESENT, 1, 0));
		act.add(new Inflection("acts", Word.VERB_3GS_PRESENT, 1, 0));
		
		LinkedList<Inflection> balance = new LinkedList<Inflection>();
		balance.add(new Inflection("balance", Word.VERB_BASE_FORM, 2, 0));
		balance.add(new Inflection("balanced", Word.VERB_PAST_TENSE, 2, 0));
		balance.add(new Inflection("balancing", Word.VERB_GERUND, 3, 0));
		balance.add(new Inflection("balanced", Word.VERB_PAST_PARTICIPLE, 2, 0));
		balance.add(new Inflection("balance", Word.VERB_NON_3GS_PRESENT, 2, 0));
		balance.add(new Inflection("balances", Word.VERB_3GS_PRESENT, 3, 0));
		
		LinkedList<Inflection> brush = new LinkedList<Inflection>();
		brush.add(new Inflection("brush", Word.VERB_BASE_FORM, 1, 0));
		brush.add(new Inflection("brushed", Word.VERB_PAST_TENSE, 2, 0));
		brush.add(new Inflection("brushing", Word.VERB_GERUND, 2, 0));
		brush.add(new Inflection("brush", Word.VERB_PAST_PARTICIPLE, 1, 0));
		brush.add(new Inflection("brush", Word.VERB_NON_3GS_PRESENT, 1, 0));
		brush.add(new Inflection("brushes", Word.VERB_3GS_PRESENT, 2, 0));
		
		LinkedList<Inflection> bandage = new LinkedList<Inflection>();
		bandage.add(new Inflection("bandage", Word.VERB_BASE_FORM, 2, 0));
		bandage.add(new Inflection("bandaged", Word.VERB_PAST_TENSE, 2, 0));
		bandage.add(new Inflection("bandaging", Word.VERB_GERUND, 3, 0));
		bandage.add(new Inflection("bandaged", Word.VERB_PAST_PARTICIPLE, 2, 0));
		bandage.add(new Inflection("bandage", Word.VERB_NON_3GS_PRESENT, 2, 0));
		bandage.add(new Inflection("bandages", Word.VERB_3GS_PRESENT, 2, 0));
		
		LinkedList<Inflection> bend = new LinkedList<Inflection>();
		bend.add(new Inflection("bend", Word.VERB_BASE_FORM, 1, 0));
		bend.add(new Inflection("bended", Word.VERB_PAST_TENSE, 2, 0));
		bend.add(new Inflection("bending", Word.VERB_GERUND, 2, 0));
		bend.add(new Inflection("bended", Word.VERB_PAST_PARTICIPLE, 2, 0));
		bend.add(new Inflection("bend", Word.VERB_NON_3GS_PRESENT, 1, 0));
		bend.add(new Inflection("bends", Word.VERB_3GS_PRESENT, 1, 0));
		
		LinkedList<Inflection> claim = new LinkedList<Inflection>();
		claim.add(new Inflection("claim", Word.VERB_BASE_FORM, 1, 0));
		claim.add(new Inflection("claimed", Word.VERB_PAST_TENSE, 2, 0));
		claim.add(new Inflection("claiming", Word.VERB_GERUND, 2, 0));
		claim.add(new Inflection("claimed", Word.VERB_PAST_PARTICIPLE, 2, 0));
		claim.add(new Inflection("claim", Word.VERB_NON_3GS_PRESENT, 1, 0));
		claim.add(new Inflection("claims", Word.VERB_3GS_PRESENT, 1, 0));
		
		LinkedList<Inflection> cover = new LinkedList<Inflection>();
		cover.add(new Inflection("cover", Word.VERB_BASE_FORM, 2, 0));
		cover.add(new Inflection("covered", Word.VERB_PAST_TENSE, 2, 0));
		cover.add(new Inflection("covering", Word.VERB_GERUND, 3, 0));
		cover.add(new Inflection("covered", Word.VERB_PAST_PARTICIPLE, 2, 0));
		cover.add(new Inflection("cover", Word.VERB_NON_3GS_PRESENT, 2, 0));
		cover.add(new Inflection("covers", Word.VERB_3GS_PRESENT, 2, 0));
		
		LinkedList<Inflection> count = new LinkedList<Inflection>();
		count.add(new Inflection("count", Word.VERB_BASE_FORM, 1, 0));
		count.add(new Inflection("counted", Word.VERB_PAST_TENSE, 2, 0));
		count.add(new Inflection("counting", Word.VERB_GERUND, 2, 0));
		count.add(new Inflection("counted", Word.VERB_PAST_PARTICIPLE, 2, 0));
		count.add(new Inflection("count", Word.VERB_NON_3GS_PRESENT, 1, 0));
		count.add(new Inflection("counts", Word.VERB_3GS_PRESENT, 1, 0));
		
		LinkedList<Inflection> cure = new LinkedList<Inflection>();
		cure.add(new Inflection("cure", Word.VERB_BASE_FORM, 1, 0));
		cure.add(new Inflection("cured", Word.VERB_PAST_TENSE, 1, 0));
		cure.add(new Inflection("curing", Word.VERB_GERUND, 2, 0));
		cure.add(new Inflection("cured", Word.VERB_PAST_PARTICIPLE, 1, 0));
		cure.add(new Inflection("cure", Word.VERB_NON_3GS_PRESENT, 1, 0));
		cure.add(new Inflection("cures", Word.VERB_3GS_PRESENT, 1, 0));
		
		LinkedList<Inflection> damage = new LinkedList<Inflection>();
		damage.add(new Inflection("damage", Word.VERB_BASE_FORM, 2, 0));
		damage.add(new Inflection("damaged", Word.VERB_PAST_TENSE, 2, 0));
		damage.add(new Inflection("damaging", Word.VERB_GERUND, 3, 0));
		damage.add(new Inflection("damaged", Word.VERB_PAST_PARTICIPLE, 2, 0));
		damage.add(new Inflection("damage", Word.VERB_NON_3GS_PRESENT, 2, 0));
		damage.add(new Inflection("damages", Word.VERB_3GS_PRESENT, 3, 0));
		
		LinkedList<Inflection> die = new LinkedList<Inflection>();
		die.add(new Inflection("die", Word.VERB_BASE_FORM, 1, 0));
		die.add(new Inflection("died", Word.VERB_PAST_TENSE, 1, 0));
		die.add(new Inflection("dying", Word.VERB_GERUND, 2, 0));
		die.add(new Inflection("died", Word.VERB_PAST_PARTICIPLE, 1, 0));
		die.add(new Inflection("die", Word.VERB_NON_3GS_PRESENT, 1, 0));
		die.add(new Inflection("dies", Word.VERB_3GS_PRESENT, 1, 0));
		
		LinkedList<Inflection> dive = new LinkedList<Inflection>();
		dive.add(new Inflection("dive", Word.VERB_BASE_FORM, 1, 0));
		dive.add(new Inflection("dived", Word.VERB_PAST_TENSE, 1, 0));
		dive.add(new Inflection("diving", Word.VERB_GERUND, 2, 0));
		dive.add(new Inflection("dived", Word.VERB_PAST_PARTICIPLE, 1, 0));
		dive.add(new Inflection("dive", Word.VERB_NON_3GS_PRESENT, 1, 0));
		dive.add(new Inflection("dives", Word.VERB_3GS_PRESENT, 1, 0));
		
		LinkedList<Inflection> echo = new LinkedList<Inflection>();
		echo.add(new Inflection("echo", Word.VERB_BASE_FORM, 2, 0));
		echo.add(new Inflection("echoed", Word.VERB_PAST_TENSE, 2, 0));
		echo.add(new Inflection("echoing", Word.VERB_GERUND, 3, 0));
		echo.add(new Inflection("echoed", Word.VERB_PAST_PARTICIPLE, 2, 0));
		echo.add(new Inflection("echo", Word.VERB_NON_3GS_PRESENT, 2, 0));
		echo.add(new Inflection("echos", Word.VERB_3GS_PRESENT, 2, 0));
		
		LinkedList<Inflection> force = new LinkedList<Inflection>();
		force.add(new Inflection("force", Word.VERB_BASE_FORM, 1, 0));
		force.add(new Inflection("forced", Word.VERB_PAST_TENSE, 1, 0));
		force.add(new Inflection("forcing", Word.VERB_GERUND, 2, 0));
		force.add(new Inflection("forced", Word.VERB_PAST_PARTICIPLE, 1, 0));
		force.add(new Inflection("force", Word.VERB_NON_3GS_PRESENT, 1, 0));
		force.add(new Inflection("forces", Word.VERB_3GS_PRESENT, 2, 0));
		
		LinkedList<Inflection> freeze = new LinkedList<Inflection>();
		freeze.add(new Inflection("freeze", Word.VERB_BASE_FORM, 1, 0));
		freeze.add(new Inflection("froze", Word.VERB_PAST_TENSE, 1, 0));
		freeze.add(new Inflection("freezing", Word.VERB_GERUND, 2, 0));
		freeze.add(new Inflection("frozen", Word.VERB_PAST_PARTICIPLE, 2, 0));
		freeze.add(new Inflection("freeze", Word.VERB_NON_3GS_PRESENT, 1, 0));
		freeze.add(new Inflection("freezes", Word.VERB_3GS_PRESENT, 2, 0));
		
		LinkedList<Inflection> grimace = new LinkedList<Inflection>();
		grimace.add(new Inflection("grimace", Word.VERB_BASE_FORM, 2, 0));
		grimace.add(new Inflection("grimaced", Word.VERB_PAST_TENSE, 2, 0));
		grimace.add(new Inflection("grimacing", Word.VERB_GERUND, 3, 0));
		grimace.add(new Inflection("grimaced", Word.VERB_PAST_PARTICIPLE, 2, 0));
		grimace.add(new Inflection("grimace", Word.VERB_NON_3GS_PRESENT, 2, 0));
		grimace.add(new Inflection("grimaces", Word.VERB_3GS_PRESENT, 2, 0));
		
		LinkedList<Inflection> grin = new LinkedList<Inflection>();
		grin.add(new Inflection("grin", Word.VERB_BASE_FORM, 1, 0));
		grin.add(new Inflection("grinned", Word.VERB_PAST_TENSE, 1, 0));
		grin.add(new Inflection("grinning", Word.VERB_GERUND, 2, 0));
		grin.add(new Inflection("grinned", Word.VERB_PAST_PARTICIPLE, 1, 0));
		grin.add(new Inflection("grin", Word.VERB_NON_3GS_PRESENT, 1, 0));
		grin.add(new Inflection("grins", Word.VERB_3GS_PRESENT, 1, 0));
		
		LinkedList<Inflection> hope = new LinkedList<Inflection>();
		hope.add(new Inflection("hope", Word.VERB_BASE_FORM, 1, 0));
		hope.add(new Inflection("hoped", Word.VERB_PAST_TENSE, 1, 0));
		hope.add(new Inflection("hoping", Word.VERB_GERUND, 2, 0));
		hope.add(new Inflection("hoped", Word.VERB_PAST_PARTICIPLE, 1, 0));
		hope.add(new Inflection("hope", Word.VERB_NON_3GS_PRESENT, 1, 0));
		hope.add(new Inflection("hopes", Word.VERB_3GS_PRESENT, 1, 0));
		
		LinkedList<Inflection> hurry = new LinkedList<Inflection>();
		hurry.add(new Inflection("hurry", Word.VERB_BASE_FORM, 2, 0));
		hurry.add(new Inflection("hurried", Word.VERB_PAST_TENSE, 2, 0));
		hurry.add(new Inflection("hurrying", Word.VERB_GERUND, 3, 0));
		hurry.add(new Inflection("hurried", Word.VERB_PAST_PARTICIPLE, 2, 0));
		hurry.add(new Inflection("hurry", Word.VERB_NON_3GS_PRESENT, 2, 0));
		hurry.add(new Inflection("hurries", Word.VERB_3GS_PRESENT, 2, 0));
		
		LinkedList<Inflection> insult = new LinkedList<Inflection>();
		insult.add(new Inflection("insult", Word.VERB_BASE_FORM, 2, 1));
		insult.add(new Inflection("insulted", Word.VERB_PAST_TENSE, 3, 1));
		insult.add(new Inflection("insulting", Word.VERB_GERUND, 3, 1));
		insult.add(new Inflection("insulted", Word.VERB_PAST_PARTICIPLE, 3, 1));
		insult.add(new Inflection("insult", Word.VERB_NON_3GS_PRESENT, 2, 1));
		insult.add(new Inflection("insults", Word.VERB_3GS_PRESENT, 2, 1));
		
		LinkedList<Inflection> kiss = new LinkedList<Inflection>();
		kiss.add(new Inflection("kiss", Word.VERB_BASE_FORM, 1, 0));
		kiss.add(new Inflection("kissed", Word.VERB_PAST_TENSE, 1, 0));
		kiss.add(new Inflection("kissing", Word.VERB_GERUND, 2, 0));
		kiss.add(new Inflection("kissed", Word.VERB_PAST_PARTICIPLE, 1, 0));
		kiss.add(new Inflection("kiss", Word.VERB_NON_3GS_PRESENT, 1, 0));
		kiss.add(new Inflection("kisses", Word.VERB_3GS_PRESENT, 2, 0));
		
		LinkedList<Inflection> land = new LinkedList<Inflection>();
		land.add(new Inflection("land", Word.VERB_BASE_FORM, 1, 0));
		land.add(new Inflection("landed", Word.VERB_PAST_TENSE, 2, 0));
		land.add(new Inflection("landing", Word.VERB_GERUND, 2, 0));
		land.add(new Inflection("landed", Word.VERB_PAST_PARTICIPLE, 2, 0));
		land.add(new Inflection("land", Word.VERB_NON_3GS_PRESENT, 1, 0));
		land.add(new Inflection("lands", Word.VERB_3GS_PRESENT, 1, 0));
		
		LinkedList<Inflection> level = new LinkedList<Inflection>();
		level.add(new Inflection("level", Word.VERB_BASE_FORM, 2, 0));
		level.add(new Inflection("leveled", Word.VERB_PAST_TENSE, 2, 0));
		level.add(new Inflection("leveling", Word.VERB_GERUND, 3, 0));
		level.add(new Inflection("leveled", Word.VERB_PAST_PARTICIPLE, 2, 0));
		level.add(new Inflection("level", Word.VERB_NON_3GS_PRESENT, 2, 0));
		level.add(new Inflection("levels", Word.VERB_3GS_PRESENT, 2, 0));
		
		LinkedList<Inflection> leap = new LinkedList<Inflection>();
		leap.add(new Inflection("leap", Word.VERB_BASE_FORM, 1, 0));
		leap.add(new Inflection("leapt", Word.VERB_PAST_TENSE, 1, 0));
		leap.add(new Inflection("leaping", Word.VERB_GERUND, 2, 0));
		leap.add(new Inflection("leapt", Word.VERB_PAST_PARTICIPLE, 1, 0));
		leap.add(new Inflection("leap", Word.VERB_NON_3GS_PRESENT, 1, 0));
		leap.add(new Inflection("leaps", Word.VERB_3GS_PRESENT, 1, 0));
		
		LinkedList<Inflection> bleed = new LinkedList<Inflection>();
		bleed.add(new Inflection("bleed", Word.VERB_BASE_FORM, 1, 0));
		bleed.add(new Inflection("bled", Word.VERB_PAST_TENSE, 1, 0));
		bleed.add(new Inflection("bleeding", Word.VERB_GERUND, 2, 0));
		bleed.add(new Inflection("bled", Word.VERB_PAST_PARTICIPLE, 1, 0));
		bleed.add(new Inflection("bleed", Word.VERB_NON_3GS_PRESENT, 1, 0));
		bleed.add(new Inflection("bleeds", Word.VERB_3GS_PRESENT, 1, 0));
		
		LinkedList<Inflection> laugh = new LinkedList<Inflection>();
		laugh.add(new Inflection("laugh", Word.VERB_BASE_FORM, 1, 0));
		laugh.add(new Inflection("laughed", Word.VERB_PAST_TENSE, 1, 0));
		laugh.add(new Inflection("laughing", Word.VERB_GERUND, 2, 0));
		laugh.add(new Inflection("laughed", Word.VERB_PAST_PARTICIPLE, 1, 0));
		laugh.add(new Inflection("laugh", Word.VERB_NON_3GS_PRESENT, 1, 0));
		laugh.add(new Inflection("laughs", Word.VERB_3GS_PRESENT, 1, 0));
		
		LinkedList<Inflection> look = new LinkedList<Inflection>();
		look.add(new Inflection("look", Word.VERB_BASE_FORM, 1, 0));
		look.add(new Inflection("looked", Word.VERB_PAST_TENSE, 1, 0));
		look.add(new Inflection("looking", Word.VERB_GERUND, 2, 0));
		look.add(new Inflection("looked", Word.VERB_PAST_PARTICIPLE, 1, 0));
		look.add(new Inflection("look", Word.VERB_NON_3GS_PRESENT, 1, 0));
		look.add(new Inflection("looks", Word.VERB_3GS_PRESENT, 1, 0));
		
		LinkedList<Inflection> love = new LinkedList<Inflection>();
		love.add(new Inflection("love", Word.VERB_BASE_FORM, 1, 0));
		love.add(new Inflection("loved", Word.VERB_PAST_TENSE, 1, 0));
		love.add(new Inflection("loving", Word.VERB_GERUND, 2, 0));
		love.add(new Inflection("loved", Word.VERB_PAST_PARTICIPLE, 1, 0));
		love.add(new Inflection("love", Word.VERB_NON_3GS_PRESENT, 1, 0));
		love.add(new Inflection("loves", Word.VERB_3GS_PRESENT, 1, 0));
		
		LinkedList<Inflection> move = new LinkedList<Inflection>();
		move.add(new Inflection("move", Word.VERB_BASE_FORM, 1, 0));
		move.add(new Inflection("moved", Word.VERB_PAST_TENSE, 1, 0));
		move.add(new Inflection("moving", Word.VERB_GERUND, 2, 0));
		move.add(new Inflection("moved", Word.VERB_PAST_PARTICIPLE, 1, 0));
		move.add(new Inflection("move", Word.VERB_NON_3GS_PRESENT, 1, 0));
		move.add(new Inflection("moves", Word.VERB_3GS_PRESENT, 1, 0));
		
		LinkedList<Inflection> need = new LinkedList<Inflection>();
		need.add(new Inflection("need", Word.VERB_BASE_FORM, 1, 0));
		need.add(new Inflection("needed", Word.VERB_PAST_TENSE, 2, 0));
		need.add(new Inflection("needing", Word.VERB_GERUND, 2, 0));
		need.add(new Inflection("needed", Word.VERB_PAST_PARTICIPLE, 2, 0));
		need.add(new Inflection("need", Word.VERB_NON_3GS_PRESENT, 1, 0));
		need.add(new Inflection("needs", Word.VERB_3GS_PRESENT, 1, 0));
		
		LinkedList<Inflection> note = new LinkedList<Inflection>();
		note.add(new Inflection("note", Word.VERB_BASE_FORM, 1, 0));
		note.add(new Inflection("noted", Word.VERB_PAST_TENSE, 2, 0));
		note.add(new Inflection("noting", Word.VERB_GERUND, 2, 0));
		note.add(new Inflection("noted", Word.VERB_PAST_PARTICIPLE, 2, 0));
		note.add(new Inflection("note", Word.VERB_NON_3GS_PRESENT, 1, 0));
		note.add(new Inflection("notes", Word.VERB_3GS_PRESENT, 1, 0));
		
		LinkedList<Inflection> object = new LinkedList<Inflection>();
		object.add(new Inflection("object", Word.VERB_BASE_FORM, 2, 1));
		object.add(new Inflection("objected", Word.VERB_PAST_TENSE, 3, 1));
		object.add(new Inflection("objecting", Word.VERB_GERUND, 3, 1));
		object.add(new Inflection("objected", Word.VERB_PAST_PARTICIPLE, 3, 1));
		object.add(new Inflection("object", Word.VERB_NON_3GS_PRESENT, 2, 1));
		object.add(new Inflection("objects", Word.VERB_3GS_PRESENT, 2, 1));
		
		LinkedList<Inflection> paint = new LinkedList<Inflection>();
		paint.add(new Inflection("paint", Word.VERB_BASE_FORM, 1, 0));
		paint.add(new Inflection("painted", Word.VERB_PAST_TENSE, 2, 0));
		paint.add(new Inflection("painting", Word.VERB_GERUND, 2, 0));
		paint.add(new Inflection("painted", Word.VERB_PAST_PARTICIPLE, 2, 0));
		paint.add(new Inflection("paint", Word.VERB_NON_3GS_PRESENT, 1, 0));
		paint.add(new Inflection("paints", Word.VERB_3GS_PRESENT, 1, 0));
		
		LinkedList<Inflection> present = new LinkedList<Inflection>();
		present.add(new Inflection("present", Word.VERB_BASE_FORM, 2, 1));
		present.add(new Inflection("presented", Word.VERB_PAST_TENSE, 3, 1));
		present.add(new Inflection("presenting", Word.VERB_GERUND, 3, 1));
		present.add(new Inflection("presented", Word.VERB_PAST_PARTICIPLE, 3, 1));
		present.add(new Inflection("present", Word.VERB_NON_3GS_PRESENT, 2, 1));
		present.add(new Inflection("presents", Word.VERB_3GS_PRESENT, 2, 1));
		
		LinkedList<Inflection> push = new LinkedList<Inflection>();
		push.add(new Inflection("push", Word.VERB_BASE_FORM, 1, 0));
		push.add(new Inflection("pushed", Word.VERB_PAST_TENSE, 1, 0));
		push.add(new Inflection("pushing", Word.VERB_GERUND, 2, 0));
		push.add(new Inflection("pushed", Word.VERB_PAST_PARTICIPLE, 1, 0));
		push.add(new Inflection("push", Word.VERB_NON_3GS_PRESENT, 1, 0));
		push.add(new Inflection("pushes", Word.VERB_3GS_PRESENT, 2, 0));
		
		LinkedList<Inflection> punch = new LinkedList<Inflection>();
		punch.add(new Inflection("punch", Word.VERB_BASE_FORM, 1, 0));
		punch.add(new Inflection("punched", Word.VERB_PAST_TENSE, 1, 0));
		punch.add(new Inflection("punching", Word.VERB_GERUND, 2, 0));
		punch.add(new Inflection("punched", Word.VERB_PAST_PARTICIPLE, 1, 0));
		punch.add(new Inflection("punch", Word.VERB_NON_3GS_PRESENT, 1, 0));
		punch.add(new Inflection("punches", Word.VERB_3GS_PRESENT, 2, 0));
		
		LinkedList<Inflection> promise = new LinkedList<Inflection>();
		promise.add(new Inflection("promise", Word.VERB_BASE_FORM, 2, 0));
		promise.add(new Inflection("promised", Word.VERB_PAST_TENSE, 2, 0));
		promise.add(new Inflection("promising", Word.VERB_GERUND, 3, 0));
		promise.add(new Inflection("promised", Word.VERB_PAST_PARTICIPLE, 2, 0));
		promise.add(new Inflection("promise", Word.VERB_NON_3GS_PRESENT, 2, 0));
		promise.add(new Inflection("promises", Word.VERB_3GS_PRESENT, 3, 0));
		
		LinkedList<Inflection> trace = new LinkedList<Inflection>();
		trace.add(new Inflection("trace", Word.VERB_BASE_FORM, 1, 0));
		trace.add(new Inflection("traced", Word.VERB_PAST_TENSE, 1, 0));
		trace.add(new Inflection("tracing", Word.VERB_GERUND, 2, 0));
		trace.add(new Inflection("traced", Word.VERB_PAST_PARTICIPLE, 1, 0));
		trace.add(new Inflection("trace", Word.VERB_NON_3GS_PRESENT, 1, 0));
		trace.add(new Inflection("traces", Word.VERB_3GS_PRESENT, 2, 0));
		
		LinkedList<Inflection> treat = new LinkedList<Inflection>();
		treat.add(new Inflection("treat", Word.VERB_BASE_FORM, 1, 0));
		treat.add(new Inflection("treated", Word.VERB_PAST_TENSE, 2, 0));
		treat.add(new Inflection("treating", Word.VERB_GERUND, 2, 0));
		treat.add(new Inflection("treated", Word.VERB_PAST_PARTICIPLE, 2, 0));
		treat.add(new Inflection("treat", Word.VERB_NON_3GS_PRESENT, 1, 0));
		treat.add(new Inflection("treats", Word.VERB_3GS_PRESENT, 1, 0));
		
		LinkedList<Inflection> walk = new LinkedList<Inflection>();
		walk.add(new Inflection("walk", Word.VERB_BASE_FORM, 1, 0));
		walk.add(new Inflection("walked", Word.VERB_PAST_TENSE, 1, 0));
		walk.add(new Inflection("walking", Word.VERB_GERUND, 2, 0));
		walk.add(new Inflection("walked", Word.VERB_PAST_PARTICIPLE, 1, 0));
		walk.add(new Inflection("walk", Word.VERB_NON_3GS_PRESENT, 1, 0));
		walk.add(new Inflection("walks", Word.VERB_3GS_PRESENT, 1, 0));
		
		LinkedList<Inflection> whisper = new LinkedList<Inflection>();
		whisper.add(new Inflection("whisper", Word.VERB_BASE_FORM, 2, 0));
		whisper.add(new Inflection("whispered", Word.VERB_PAST_TENSE, 2, 0));
		whisper.add(new Inflection("whispering", Word.VERB_GERUND, 3, 0));
		whisper.add(new Inflection("whispered", Word.VERB_PAST_PARTICIPLE, 2, 0));
		whisper.add(new Inflection("whisper", Word.VERB_NON_3GS_PRESENT, 2, 0));
		whisper.add(new Inflection("whispers", Word.VERB_3GS_PRESENT, 2, 0));
		
		LinkedList<Inflection> sound = new LinkedList<Inflection>();
		sound.add(new Inflection("sound", Word.VERB_BASE_FORM, 1, 0));
		sound.add(new Inflection("sounded", Word.VERB_PAST_TENSE, 2, 0));
		sound.add(new Inflection("sounding", Word.VERB_GERUND, 2, 0));
		sound.add(new Inflection("sounded", Word.VERB_PAST_PARTICIPLE, 2, 0));
		sound.add(new Inflection("sound", Word.VERB_NON_3GS_PRESENT, 1, 0));
		sound.add(new Inflection("sounds", Word.VERB_3GS_PRESENT, 1, 0));
		
		LinkedList<Inflection> whish = new LinkedList<Inflection>();
		whish.add(new Inflection("whish", Word.VERB_BASE_FORM, 1, 0));
		whish.add(new Inflection("whished", Word.VERB_PAST_TENSE, 1, 0));
		whish.add(new Inflection("whishing", Word.VERB_GERUND, 2, 0));
		whish.add(new Inflection("whished", Word.VERB_PAST_PARTICIPLE, 2, 0));
		whish.add(new Inflection("whish", Word.VERB_NON_3GS_PRESENT, 1, 0));
		whish.add(new Inflection("whishes", Word.VERB_3GS_PRESENT, 2, 0));
		
		words.add(new Word("ache", ache));
		words.add(new Word("alert", alert));
		words.add(new Word("answer", answer));
		words.add(new Word("act", act));
		words.add(new Word("balance", balance));
		words.add(new Word("brush", brush));
		words.add(new Word("bandage", bandage));
		words.add(new Word("bend", bend));
		words.add(new Word("claim", claim));
		words.add(new Word("cover", cover));
		words.add(new Word("count", count));
		words.add(new Word("cure", cure));
		words.add(new Word("damage", damage));
		words.add(new Word("die", die));
		words.add(new Word("dive", dive));
		words.add(new Word("echo", echo));
		words.add(new Word("force", force));
		words.add(new Word("freeze", freeze));
		words.add(new Word("grimace", grimace));
		words.add(new Word("grin", grin));
		words.add(new Word("hope", hope));
		words.add(new Word("hurry", hurry));
		words.add(new Word("insult", insult));
		words.add(new Word("kiss", kiss));
		words.add(new Word("land", land));
		words.add(new Word("level", level));
		words.add(new Word("leap", leap));
		words.add(new Word("bleed", bleed));
		words.add(new Word("laugh", laugh));
		words.add(new Word("look", look));
		words.add(new Word("love", love));
		words.add(new Word("move", move));
		words.add(new Word("need", need));
		words.add(new Word("note", note));
		words.add(new Word("object", object));
		words.add(new Word("paint", paint));
		words.add(new Word("present", present));
		words.add(new Word("push", push));
		words.add(new Word("punch", punch));
		words.add(new Word("promise", promise));
		words.add(new Word("trace", trace));
		words.add(new Word("treat", treat));
		words.add(new Word("walk", walk));
		words.add(new Word("whisper", whisper));
		words.add(new Word("sound", sound));
		words.add(new Word("whish", whish));
		
		createFeatureMap();
	}
	
	protected void createFeatureMap()
	{
		this.avm = this.getAgreeAllAvm();
	}
	
}
