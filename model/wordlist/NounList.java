package model.wordlist;

import java.util.LinkedList;

import model.word.Inflection;
import model.word.Word;

public class NounList extends WordList {
	
	public NounList()
	{
		LinkedList<Inflection> ache = new LinkedList<Inflection>();
		ache.add(new Inflection("ache", Word.NOUN_SINGULAR, 1, 0));
		ache.add(new Inflection("aches", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> alert = new LinkedList<Inflection>();
		alert.add(new Inflection("alert", Word.NOUN_SINGULAR, 2, 1));
		alert.add(new Inflection("alerts", Word.NOUN_PLURAL, 2, 1));
		
		LinkedList<Inflection> answer = new LinkedList<Inflection>();
		answer.add(new Inflection("answer", Word.NOUN_SINGULAR, 2, 0));
		answer.add(new Inflection("answers", Word.NOUN_PLURAL, 2, 0));
		
		LinkedList<Inflection> act = new LinkedList<Inflection>();
		act.add(new Inflection("act", Word.NOUN_SINGULAR, 1, 0));
		act.add(new Inflection("acts", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> balance = new LinkedList<Inflection>();
		balance.add(new Inflection("balance", Word.NOUN_SINGULAR, 2, 0));
		balance.add(new Inflection("balances", Word.NOUN_PLURAL, 3, 0));
		
		LinkedList<Inflection> brush = new LinkedList<Inflection>();
		brush.add(new Inflection("brush", Word.NOUN_SINGULAR, 1, 0));
		brush.add(new Inflection("brushes", Word.NOUN_PLURAL, 2, 0));
		
		LinkedList<Inflection> bandage = new LinkedList<Inflection>();
		bandage.add(new Inflection("bandage", Word.NOUN_SINGULAR, 2, 0));
		bandage.add(new Inflection("bandages", Word.NOUN_PLURAL, 3, 0));
		
		LinkedList<Inflection> bend = new LinkedList<Inflection>();
		bend.add(new Inflection("bend", Word.NOUN_SINGULAR, 1, 0));
		bend.add(new Inflection("bends", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> claim = new LinkedList<Inflection>();
		claim.add(new Inflection("claim", Word.NOUN_SINGULAR, 1, 0));
		claim.add(new Inflection("claims", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> contrast = new LinkedList<Inflection>();
		contrast.add(new Inflection("contrast", Word.NOUN_SINGULAR, 2, 0));
		contrast.add(new Inflection("contrasts", Word.NOUN_PLURAL, 2, 0));
		
		LinkedList<Inflection> cover = new LinkedList<Inflection>();
		cover.add(new Inflection("cover", Word.NOUN_SINGULAR, 2, 0));
		cover.add(new Inflection("covers", Word.NOUN_PLURAL, 2, 0));
		
		LinkedList<Inflection> count = new LinkedList<Inflection>();
		count.add(new Inflection("count", Word.NOUN_SINGULAR, 1, 0));
		count.add(new Inflection("counts", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> cure = new LinkedList<Inflection>();
		cure.add(new Inflection("cure", Word.NOUN_SINGULAR, 1, 0));
		cure.add(new Inflection("cures", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> damage = new LinkedList<Inflection>();
		damage.add(new Inflection("damage", Word.NOUN_SINGULAR, 2, 0));
		damage.add(new Inflection("damages", Word.NOUN_PLURAL, 3, 0));
		
		LinkedList<Inflection> die = new LinkedList<Inflection>();
		die.add(new Inflection("dye", Word.NOUN_SINGULAR, 1, 0));
		die.add(new Inflection("dyes", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> dive = new LinkedList<Inflection>();
		dive.add(new Inflection("dive", Word.NOUN_SINGULAR, 1, 0));
		dive.add(new Inflection("dives", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> echo = new LinkedList<Inflection>();
		echo.add(new Inflection("echo", Word.NOUN_SINGULAR, 1, 0));
		echo.add(new Inflection("echos", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> force = new LinkedList<Inflection>();
		force.add(new Inflection("force", Word.NOUN_SINGULAR, 1, 0));
		force.add(new Inflection("forces", Word.NOUN_PLURAL, 2, 0));
		
		LinkedList<Inflection> freeze = new LinkedList<Inflection>();
		freeze.add(new Inflection("freeze", Word.NOUN_SINGULAR, 1, 0));
		freeze.add(new Inflection("freezes", Word.NOUN_PLURAL, 2, 0));
		
		LinkedList<Inflection> grimace = new LinkedList<Inflection>();
		grimace.add(new Inflection("grimace", Word.NOUN_SINGULAR, 2, 0));
		grimace.add(new Inflection("grimaces", Word.NOUN_PLURAL, 3, 0));
		
		LinkedList<Inflection> grin = new LinkedList<Inflection>();
		grin.add(new Inflection("grin", Word.NOUN_SINGULAR, 1, 0));
		grin.add(new Inflection("grins", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> hope = new LinkedList<Inflection>();
		hope.add(new Inflection("hope", Word.NOUN_SINGULAR, 1, 0));
		hope.add(new Inflection("hopes", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> hurry = new LinkedList<Inflection>();
		hurry.add(new Inflection("hurry", Word.NOUN_SINGULAR, 2, 0));
		hurry.add(new Inflection("hurries", Word.NOUN_PLURAL, 2, 0));
		
		LinkedList<Inflection> insult = new LinkedList<Inflection>();
		insult.add(new Inflection("insult", Word.NOUN_SINGULAR, 2, 1));
		insult.add(new Inflection("insults", Word.NOUN_PLURAL, 2, 1));
		
		LinkedList<Inflection> kiss = new LinkedList<Inflection>();
		kiss.add(new Inflection("kiss", Word.NOUN_SINGULAR, 1, 0));
		kiss.add(new Inflection("kisses", Word.NOUN_PLURAL, 2, 0));
		
		LinkedList<Inflection> land = new LinkedList<Inflection>();
		land.add(new Inflection("land", Word.NOUN_SINGULAR, 1, 0));
		land.add(new Inflection("lands", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> level = new LinkedList<Inflection>();
		level.add(new Inflection("level", Word.NOUN_SINGULAR, 2, 0));
		level.add(new Inflection("levels", Word.NOUN_PLURAL, 2, 0));
		
		LinkedList<Inflection> leap = new LinkedList<Inflection>();
		leap.add(new Inflection("leap", Word.NOUN_SINGULAR, 1, 0));
		leap.add(new Inflection("leaps", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> laugh = new LinkedList<Inflection>();
		laugh.add(new Inflection("laugh", Word.NOUN_SINGULAR, 1, 0));
		laugh.add(new Inflection("laughs", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> look = new LinkedList<Inflection>();
		look.add(new Inflection("look", Word.NOUN_SINGULAR, 1, 0));
		look.add(new Inflection("looks", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> love = new LinkedList<Inflection>();
		love.add(new Inflection("love", Word.NOUN_SINGULAR, 1, 0));
		love.add(new Inflection("loves", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> mate = new LinkedList<Inflection>();
		mate.add(new Inflection("mate", Word.NOUN_SINGULAR, 1, 0));
		mate.add(new Inflection("mates", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> mine = new LinkedList<Inflection>();
		mine.add(new Inflection("mine", Word.NOUN_SINGULAR, 1, 0));
		mine.add(new Inflection("mines", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> move = new LinkedList<Inflection>();
		move.add(new Inflection("move", Word.NOUN_SINGULAR, 1, 0));
		move.add(new Inflection("moves", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> nail = new LinkedList<Inflection>();
		nail.add(new Inflection("nail", Word.NOUN_SINGULAR, 1, 0));
		nail.add(new Inflection("nails", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> need = new LinkedList<Inflection>();
		need.add(new Inflection("need", Word.NOUN_SINGULAR, 1, 0));
		need.add(new Inflection("needs", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> note = new LinkedList<Inflection>();
		note.add(new Inflection("note", Word.NOUN_SINGULAR, 1, 0));
		note.add(new Inflection("notes", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> object = new LinkedList<Inflection>();
		object.add(new Inflection("object", Word.NOUN_SINGULAR, 2, 0));
		object.add(new Inflection("objects", Word.NOUN_PLURAL, 2, 0));
		
		LinkedList<Inflection> paint = new LinkedList<Inflection>();
		paint.add(new Inflection("paint", Word.NOUN_SINGULAR, 1, 0));
		paint.add(new Inflection("paints", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> present = new LinkedList<Inflection>();
		present.add(new Inflection("present", Word.NOUN_SINGULAR, 2, 0));
		present.add(new Inflection("presents", Word.NOUN_PLURAL, 2, 0));
		
		LinkedList<Inflection> push = new LinkedList<Inflection>();
		push.add(new Inflection("push", Word.NOUN_SINGULAR, 1, 0));
		push.add(new Inflection("pushes", Word.NOUN_PLURAL, 2, 0));
		
		LinkedList<Inflection> punch = new LinkedList<Inflection>();
		punch.add(new Inflection("punch", Word.NOUN_SINGULAR, 1, 0));
		punch.add(new Inflection("punches", Word.NOUN_PLURAL, 2, 0));
		
		LinkedList<Inflection> promise = new LinkedList<Inflection>();
		promise.add(new Inflection("promise", Word.NOUN_SINGULAR, 2, 0));
		promise.add(new Inflection("promises", Word.NOUN_PLURAL, 3, 0));
		
		LinkedList<Inflection> trace = new LinkedList<Inflection>();
		trace.add(new Inflection("trace", Word.NOUN_SINGULAR, 1, 0));
		trace.add(new Inflection("traces", Word.NOUN_PLURAL, 2, 0));
		
		LinkedList<Inflection> treat = new LinkedList<Inflection>();
		treat.add(new Inflection("treat", Word.NOUN_SINGULAR, 1, 0));
		treat.add(new Inflection("treats", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> walk = new LinkedList<Inflection>();
		walk.add(new Inflection("walk", Word.NOUN_SINGULAR, 1, 0));
		walk.add(new Inflection("walks", Word.NOUN_PLURAL, 2, 0));
		
		LinkedList<Inflection> whisper = new LinkedList<Inflection>();
		whisper.add(new Inflection("whisper", Word.NOUN_SINGULAR, 2, 0));
		whisper.add(new Inflection("whispers", Word.NOUN_PLURAL, 2, 0));
		
		LinkedList<Inflection> sound = new LinkedList<Inflection>();
		sound.add(new Inflection("sound", Word.NOUN_SINGULAR, 1, 0));
		sound.add(new Inflection("sounds", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> whish = new LinkedList<Inflection>();
		whish.add(new Inflection("whish", Word.NOUN_SINGULAR, 1, 0));
		whish.add(new Inflection("whishes", Word.NOUN_PLURAL, 2, 0));
		
		LinkedList<Inflection> boy = new LinkedList<Inflection>();
		boy.add(new Inflection("boy", Word.NOUN_SINGULAR, 1, 0));
		boy.add(new Inflection("boys", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> girl = new LinkedList<Inflection>();
		girl.add(new Inflection("girl", Word.NOUN_SINGULAR, 1, 0));
		girl.add(new Inflection("girls", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> man = new LinkedList<Inflection>();
		man.add(new Inflection("man", Word.NOUN_SINGULAR, 1, 0));
		man.add(new Inflection("men", Word.NOUN_PLURAL, 1, 0));
		
		LinkedList<Inflection> woman = new LinkedList<Inflection>();
		woman.add(new Inflection("woman", Word.NOUN_SINGULAR, 2, 0));
		woman.add(new Inflection("women", Word.NOUN_PLURAL, 2, 0));
		
		words.add(new Word("ache", ache));
		words.add(new Word("alert", alert));
		words.add(new Word("answer", answer));
		words.add(new Word("act", act));
		words.add(new Word("balance", balance));
		words.add(new Word("brush", brush));
		words.add(new Word("bandage", bandage));
		words.add(new Word("bend", bend));
		words.add(new Word("claim", claim));
		words.add(new Word("contrast", contrast));
		words.add(new Word("cover", cover));
		words.add(new Word("count", count));
		words.add(new Word("cure", cure));
		words.add(new Word("damage", damage));
		words.add(new Word("die", die));
		words.add(new Word("dive", dive));
		words.add(new Word("echo", echo));
		words.add(new Word("force", force));
		words.add(new Word("freeze", freeze));
		words.add(new Word("grimace", grimace));
		words.add(new Word("grin", grin));
		words.add(new Word("hope", hope));
		words.add(new Word("hurry", hurry));
		words.add(new Word("insult", insult));
		words.add(new Word("kiss", kiss));
		words.add(new Word("land", land));
		words.add(new Word("level", level));
		words.add(new Word("leap", leap));
		words.add(new Word("laugh", laugh));
		words.add(new Word("look", look));
		words.add(new Word("love", love));
		words.add(new Word("mate", mate));
		words.add(new Word("mine", mine));
		words.add(new Word("move", move));
		words.add(new Word("nail", nail));
		words.add(new Word("need", need));
		words.add(new Word("note", note));
		words.add(new Word("object", object));
		words.add(new Word("paint", paint));
		words.add(new Word("present", present));
		words.add(new Word("push", push));
		words.add(new Word("punch", punch));
		words.add(new Word("promise", promise));
		words.add(new Word("trace", trace));
		words.add(new Word("treat", treat));
		words.add(new Word("walk", walk));
		words.add(new Word("whisper", whisper));
		words.add(new Word("sound", sound));
		words.add(new Word("whish", whish));
		words.add(new Word("boy", boy));
		words.add(new Word("girl", girl));
		words.add(new Word("man", man));
		words.add(new Word("woman", woman));

		createFeatureMap();
	}

	protected void createFeatureMap() {
		this.avm = this.getAgreeAllAvm();
	}

}
