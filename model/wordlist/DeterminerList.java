package model.wordlist;

import java.util.LinkedList;

import model.grammar.feature.AttributeValueMatrix;
import model.word.*;

public class DeterminerList extends WordList {

	public DeterminerList()
	{
		LinkedList<Inflection> a = new LinkedList<Inflection>();
		a.add(new Inflection("a", Word.DT_BEFORE_CONSONANT, 1, 0));
		a.add(new Inflection("an", Word.DT_BEFORE_VOWEL, 1, 0));
		a.add(new Inflection("the", Word.DT_BEFORE_BOTH, 1, 0));
		
		words.add(new Word("a", a));

		createFeatureMap();
	}

	protected void createFeatureMap() {
		this.avm = new AttributeValueMatrix();
	}
	
	public AttributeValueMatrix getAvm()
	{
		return this.avm;
	}
}
