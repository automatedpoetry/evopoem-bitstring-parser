package model.poems;

import java.util.LinkedList;

public class Poem implements Comparable<Poem> {
	private LinkedList<Line> lines;
	private int score = 0;
	
	public Poem()
	{
		lines = new LinkedList<Line>();
	}
	
	public void addLine(LinkedList<String> line, int ident)
	{
		lines.add(new Line(line, ident));
	}
	/**
	 * Get the line at the given index.
	 * @param index The index of the desired line.
	 * @return the line at the given index, if existent; null otherwise.
	 */
	public String getLine(int index) {
		if(index < 0 || index >= lines.size())
		{
			throw new IndexOutOfBoundsException();
		}
		
		Line line = lines.get(index);
		
		String s = "";
		for(int i = 0; i < line.getIdent(); i++)
		{
			s += "    ";
		}
		for(int i = 0; i < line.getLine().size(); i++)
		{
			s += String.format("%s ", line.getLine().get(i));
		}
		return s;
	}

	public int getLineCount() {
		return lines.size();
	}
	
	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}
	
	public int compareTo(Poem otherPoem) {
		return score < otherPoem.score ? -1 : (score > otherPoem.score ? 1 : 0);
	}
	
	public String toString()
	{
		String poem = "";
		for(int i = 0; i < lines.size(); i++)
		{
			poem += String.format("%s\n", getLine(i));
		}
		return poem;
	}
	
	private class Line
	{
		LinkedList<String> line;
		int ident;
		
		public Line(LinkedList<String> line, int ident)
		{
			this.line = line;
			this.ident = ident;
		}
		
		public int getIdent()
		{
			return ident;
		}
		
		public LinkedList<String> getLine()
		{
			return this.line;
		}
	}
}
