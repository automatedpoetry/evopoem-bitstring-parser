package model.grammar;

import model.grammar.feature.*;

public class ClauseFeatureTuple {
	
	private String name;
	private Clause clause;
	private AttributeValueMatrix avm;

	public ClauseFeatureTuple(String name, Clause clause, AttributeValueMatrix avm)
	{
		this.name = name;
		this.clause = clause;
		this.avm = avm;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public Clause getClause()
	{
		return this.clause;
	}
	
	public AttributeValueMatrix getAvm()
	{
		return this.avm;
	}
	
	public void setClause(Clause clause)
	{
		this.clause = clause;
	}
	
	public void setAvm(AttributeValueMatrix avm)
	{
		this.avm = avm;
	}
}
