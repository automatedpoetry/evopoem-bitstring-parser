package model.grammar.feature;

import java.util.LinkedList;
import controller.BitStringIterator;
import controller.exception.FeatureTypesDontMatchException;
import controller.exception.FeatureUnknownException;
import controller.exception.InvalidFeatureValueTypeException;

public class AttributeValueMatrix implements Cloneable{
	
	private LinkedList<Feature> avm;
	
	/**
	 * Construct an empty Attribute Value Matrix
	 */
	public AttributeValueMatrix()
	{
		avm = new LinkedList<Feature>();
	}
	
	/**
	 * Check if a feature is present in the Attribute Value Matrix
	 * @param feature	Feature name
	 * @return			True iff feature exists
	 */
	public boolean hasFeature(String feature)
	{
		String[] featureMap = feature.split("\\.", 2);
		Feature found = null;
		boolean hasFeature = false;
		for(Feature f : avm)
		{
			if(f.getName() == featureMap[0]) {
				found = f; 
				break;
			}
		}
		if(found != null)
		{
			if(featureMap.length == 1)
			{
				hasFeature = true;
			} else if(found.getType() == Feature.TYPE_AVM) {
				try {
					hasFeature = found.getAvm().hasFeature(featureMap[1]);
				} catch (InvalidFeatureValueTypeException e) {
					e.printStackTrace();
				}
			}
		}
		
		return hasFeature;
	}
	
	/**
	 * Method to add a feature to the attribute value matrix
	 * @param feature	Feature to be added
	 * @return			True iff feature could be added, false
	 * 					if feature with the name already exists
	 */
	public boolean addFeature(Feature feature)
	{
		for(Feature f : avm)
		{
			if(f.getName() == feature.getName())
			{
				return false;
			}
		}
		avm.add(feature);
		return true;
	}
	
	/**
	 * Get the feature for a feature name
	 * @param feature	Feature name. If nested, seperated by dots
	 * @return			Feature if feature for name exists, exception otherwise
	 * @throws FeatureUnknownException 
	 */
	public Feature getFeature(String feature) throws FeatureUnknownException
	{
		// General trackers
		Feature foundFeature = null;
		boolean found = false;
		
		// Split path by dots
		String[] featureMap = feature.split("\\.", 2);
		
		// Check if feature exists in avm with feature name
		for(Feature f : avm)
		{
			if(f.getName().equals(featureMap[0])) {
				foundFeature = f;
				found = true;
				break;
			}
		}
		
		// Try to recursively find the final feature
		if(found && featureMap.length > 1 && foundFeature.getType() == Feature.TYPE_AVM)
		{
			try {
				foundFeature = foundFeature.getAvm().getFeature(featureMap[1]);
			} catch (InvalidFeatureValueTypeException e) {
				found = false;
			}
		}
		
		if(!found) 
		{
			throw new FeatureUnknownException(feature);
		}
		
		return foundFeature;
	}
	
	/**
	 * Unify a matrix with the current attribute value matrix by recursively 
	 * unifying all elements of the matrices
	 * @param avm1	Attribute Value Matrix to be unified with current avm
	 * @return		New attribute Value Matrix that is the unification of
	 * 				the current avm and the argument avm, if they could be
	 * 				unified
	 * @throws InvalidFeatureValueTypeException 
	 * @throws FeatureTypesDontMatchException 
	 */
	public AttributeValueMatrix unify(AttributeValueMatrix avm1) 
			throws FeatureTypesDontMatchException, InvalidFeatureValueTypeException
	{
		LinkedList<Feature> nAvm = new LinkedList<Feature>();
		for(Feature f : avm)
		{
			if(avm1.hasFeature(f.getName())) {
				try {
					nAvm.add(f.unify(avm1.getFeature(f.getName())));
				} catch (FeatureUnknownException e) {
					e.printStackTrace();
				}
			} else {
				nAvm.add(f);
			}
		}
		for(Feature f : avm1.avm)
		{
			if(!this.hasFeature(f.getName()))
			{
				nAvm.add(f);
			}
		}
		
		AttributeValueMatrix unified = new AttributeValueMatrix();
		unified.avm = nAvm;
		return unified;
	}
	
	/**
	 * Method that returns a clone of the current avm
	 */
	public AttributeValueMatrix clone()
	{
		AttributeValueMatrix navm = new AttributeValueMatrix();
		navm.avm = new LinkedList<Feature>();
		this.avm.forEach(feature -> navm.avm.add(feature.clone()));
		return navm;
	}
	
	/**
	 * Prettyprints the current AMV
	 */
	public String toString()
	{
		return this.toString("");
	}
	
	/**
	 * Recursively prints each part of the AVM
	 * @param ident
	 * @return
	 */
	public String toString(String ident)
	{
		String nIdent = String.format("    %s", ident);
		String result = String.format("%s", ident);
		for(Feature f : avm)
		{
			result += String.format("%s", f.toString(nIdent));
		}
		result += String.format("\n%s]", ident);
		return result;
	}
	
	/**
	 * Method to fix the domain of all values
	 * @param it	Bit string iterator object
	 */
	public void fixAllDomains(BitStringIterator it)
	{
		for(Feature f : avm)
		{
			f.fixDomain(it);
		}
	}
	
}
