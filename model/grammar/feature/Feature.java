package model.grammar.feature;

import java.util.Arrays;

import controller.BitStringIterator;
import controller.exception.FeatureTypesDontMatchException;
import controller.exception.InvalidFeatureValueTypeException;
import controller.exception.ValueNotInstantiatedException;

import java.util.ArrayList;

/**
 * A feature can consist of a string or another feature
 * Domain of a feature can be an integer, a string or another AVM
 */
public class Feature implements Cloneable{
	
	protected static final int TYPE_INT = 0, TYPE_STRING = 1, TYPE_AVM = 2; 
	
	private final String name;
	private int type;
	private AttributeValueMatrix valueAvm;
	
	// Keeps track if the domain is restricted to a single variable
	private boolean fixed;
	
	private ArrayList<Integer> domain_I;
	private ArrayList<String> domain_S;
	// Don't allow domains of AVM's, as the domain is of a single field only
	
	/**
	 * Construct a new feature with an integer as value or integers as value domain
	 * @param name		Feature name
	 * @param values	Feature value or possible feature values
	 */
	public Feature(String name, Integer...values)
	{
		this.type = TYPE_INT;
		this.name = name;
		this.domain_I = new ArrayList<Integer>(Arrays.asList(values));
		this.fixed = domain_I.size() == 1;
	}
	
	/**
	 * Construct a new feature with a String as value or Strings as value domain
	 * @param name		Feature name
	 * @param values	Feature value or possible feature values
	 */
	public Feature(String name, String...value)
	{
		this.type = TYPE_STRING;
		this.name = name;
		this.domain_S = new ArrayList<String>(Arrays.asList(value));
		fixed = this.domain_S.size() == 1;
	}
	
	/**
	 * Construct a new feature with an Attribute Value Matrix as value
	 * @param name		Feature name
	 * @param values	Feature value
	 */
	public Feature(String name, AttributeValueMatrix value)
	{
		this.type = TYPE_AVM;
		this.name = name;
		this.valueAvm = value;
		// TODO optimalisation by keeping track whether the AVM is fixed? Or don't bother?
	}
	
	protected int getType()
	{
		return this.type;
	}
	/**
	 * Method to get the feature name
	 * @return	String 	feature name
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Add values to the domain
	 * @param <E>
	 * @param values	Values of a single type
	 * @throws InvalidFeatureValueTypeException if argument type is not allowed as feature value
	 */
	@SuppressWarnings("unchecked")
	public <E> void extendDomain(E...values) throws InvalidFeatureValueTypeException
	{
		if(values instanceof Integer[] && this.type == TYPE_INT)
		{
			extendDomain((Integer[]) values);
		} else if (values instanceof String[] && this.type == TYPE_STRING) 
		{
			extendDomain((String[]) values);
		} else {
			throw new InvalidFeatureValueTypeException();
		}
	}
	
	private void extendDomain(Integer...values) throws InvalidFeatureValueTypeException
	{
		for(Integer i : values)
		{
			if(!domain_I.contains(i)) domain_I.add(i);
		}
		fixed = domain_I.size() == 1;
	}
	
	private void extendDomain(String...values) throws InvalidFeatureValueTypeException
	{
		for(String s : values)
		{
			if(!domain_S.contains(s)) domain_S.add(s);
		}
		fixed = domain_S.size() == 1;
	}
	
	@SuppressWarnings("unchecked")
	public <E> void eliminate(E...values) throws InvalidFeatureValueTypeException
	{
		if(values instanceof Integer[] && this.type == TYPE_INT)
		{
			eliminate((Integer[]) values);
		} else if (values instanceof String[] && this.type == TYPE_STRING) 
		{
			eliminate((String[]) values);
		} else {
			throw new InvalidFeatureValueTypeException();
		}
	}
	
	private void eliminate(Integer...values)
	{
		for(int i : values)
		{
			domain_I.remove(i);
		}
		fixed = domain_I.size() == 1;
	}
	
	private void eliminate(String...values)
	{
		for(String s : values)
		{
			domain_S.remove(s);
		}
		fixed = domain_S.size() == 1;
	}
	
	/**
	 * Method to assign a single value to the value of the feature
	 * Clears the rest of the values from the domain.
	 * Only Strings or Integers accepted. From Attribute Value Matrices,
	 * the individual features inside that matrix can have a domain
	 * @param value		Value to be assigned, type Integer or String
	 * @return			True if value could be assigned, false if the value
	 * 					is not currently in the domain
	 * @throws InvalidFeatureValueTypeException	if the value is not of type
	 * 					integer or String
	 */
	public <E> boolean assign(E value) throws InvalidFeatureValueTypeException
	{
		if(value instanceof Integer && this.type == TYPE_INT)
		{
			if(domain_I.contains(value)) 
			{
				domain_I = new ArrayList<Integer>(Arrays.asList((Integer) value));
				fixed = true;
				return true;
			} else return false;
		} else if(value instanceof String && this.type == TYPE_STRING)
		{
			if(domain_S.contains(value))
			{
				domain_S = new ArrayList<String>(Arrays.asList((String) value));
				fixed = true;
				return true;
			} else return false;
		} else {
			throw new InvalidFeatureValueTypeException();
		}
	}
	
	/**
	 * Method to get the feature value
	 * @return E	Feature value
	 * @throws ValueNotInstantiatedException 
	 * @throws InvalidFeatureValueTypeException 
	 */
	public Object getValue() throws ValueNotInstantiatedException, InvalidFeatureValueTypeException
	{
		Object result = null;
		
		if(this.type == Feature.TYPE_AVM)
			throw new InvalidFeatureValueTypeException(
					"Value of feature of type AttributeValueMatrix cannot be requested. "
					+ "Request value of feature in the Attribute Value Matrix instead");
		else if(!fixed)
			throw new ValueNotInstantiatedException();
		
		if(type == TYPE_INT) result = domain_I.get(0);
		else if(type == TYPE_STRING) result = domain_S.get(0);
		return result;
	}
	
	public AttributeValueMatrix getAvm() throws InvalidFeatureValueTypeException
	{
		if(this.type == TYPE_AVM) return this.valueAvm;
		throw new InvalidFeatureValueTypeException();
	}
	
	/**
	 * Method to unify a feature with this feature
	 * @param feature		Feature to unify with the current feature
	 * @return				New unified feature if features could be
	 * 						unified
	 * @throws FeatureTypesDontMatchException if feature types don't match
	 * @throws InvalidFeatureValueTypeException if feature value type does not exist
	 */
	public Feature unify(Feature feature) throws FeatureTypesDontMatchException, InvalidFeatureValueTypeException
	{
		Feature f = null;
		if(this.type != feature.type)
		{
			throw new FeatureTypesDontMatchException(type, feature.type);
		} else if(type == TYPE_INT)
		{
			f = unifyIntegerFeature(feature);
		} else if(type == TYPE_STRING)
		{
			f = unifyStringFeature(feature);
		} else if(type == TYPE_AVM)
		{
			f = unifyAvmFeature(feature);
		} else {
			throw new InvalidFeatureValueTypeException();
		}
		return f;
	}
	
	private Feature unifyIntegerFeature(Feature feature)
	{
		ArrayList<Integer> unifiedIntList = new ArrayList<Integer>();
		this.domain_I.forEach(i -> {
			if(feature.domain_I.contains(i))
			{
				unifiedIntList.add(i);
			}
		});
		
		Integer[] unifiedIntArray = new Integer[unifiedIntList.size()];
		for(int i = 0; i < unifiedIntList.size(); i++)
			unifiedIntArray[i] = unifiedIntList.get(i);
		
		Feature f = new Feature(this.name, unifiedIntArray);
		return f;
	}
	
	private Feature unifyStringFeature(Feature feature)
	{
		ArrayList<String> unifiedStringList = new ArrayList<String>();
		this.domain_S.forEach(i -> {
			if(feature.domain_S.contains(i))
			{
				unifiedStringList.add(i);
			}
		});
		
		String[] unifiedStringArray = new String[unifiedStringList.size()];
		for(int i = 0; i < unifiedStringList.size(); i++)
			unifiedStringArray[i] = unifiedStringList.get(i);
		Feature f = new Feature(this.name, unifiedStringArray);
		return f;
	}
	
	private Feature unifyAvmFeature(Feature feature) throws FeatureTypesDontMatchException, InvalidFeatureValueTypeException
	{
		AttributeValueMatrix unifiedAvm = this.valueAvm.unify(feature.valueAvm);
		Feature f = new Feature(name, unifiedAvm);
		return f;
	}
	
	/**
	 * Returns a string of the feature value type
	 * @param feature_type	internal integer representation of feature value type
	 * @return				Name of feature value type
	 */
	public static String getFeatureNameForFeatureType(int feature_type)
	{
		String s;
		if(feature_type == TYPE_INT) s = "Integer";
		else if(feature_type == TYPE_STRING) s = "String";
		else if(feature_type == TYPE_AVM) s = "Attribute Value Matrix";
		else s = "<invalid>";
		return s;
	}
	
	public Feature clone()
	{
		Feature nf;
		
		if(this.domain_I != null)
		{
			Integer[] clonedIntegerList = new Integer[this.domain_I.size()];
			for(int i = 0; i < this.domain_I.size(); i++) 
				clonedIntegerList[i] = this.domain_I.get(i).intValue();
			nf = new Feature(this.name, clonedIntegerList);
		}
		if(this.domain_S != null)
		{
			String[] clonedStringList = new String[this.domain_S.size()];
			for(int i = 0; i < this.domain_S.size(); i++)
				clonedStringList[i] = this.domain_S.get(i);
			nf = new Feature(this.name, clonedStringList);
		} else {
			nf = new Feature(this.name, this.valueAvm.clone());
		}
		nf.type = this.type;
		nf.fixed = this.fixed;
		
		return nf;
	}
	
	public String toString()
	{
		return this.toString("");
	}
	
	public String toString(String ident)
	{
		String val;
		switch(this.type)
		{
			case TYPE_INT: 
				val = this.domain_I.toString(); 
				break;
			case TYPE_STRING: 
				val = this.domain_S.toString(); 
				break;
			case TYPE_AVM: 
				val = this.valueAvm.toString(String.format("\n    %s", ident)); 
				break;
			default:
				val = "<unknown>";
		}
		return String.format("%s[%s: %s]", ident, this.name, val);
	}
	
	public void fixDomain(BitStringIterator it)
	{
		if(this.type == TYPE_INT)
		{
			try {
				this.assign(this.domain_I.get(it.next() % this.domain_I.size()));
			} catch (InvalidFeatureValueTypeException e) {
				System.out.println("If this happens, an int that's in the domain can't be assigned. Check assign function");
				e.printStackTrace();
			}
		} else if(this.type == TYPE_STRING)
		{
			try {
				this.assign(this.domain_S.get(it.next() % this.domain_S.size()));
			} catch (InvalidFeatureValueTypeException e) {
				System.out.println("If this happens, a string that's in the domain can't be assigned. Check assign function");
				e.printStackTrace();
			}
		} else {
			this.valueAvm.fixAllDomains(it);
		}
	}

}
