package model.grammar;

import model.wordlist.*;
/**
 * Contains the grammar rules for a poem and methods to generate
 * a poem based on a Bit string object
 */
public class Grammar {
	
	/**
	 * Lexicon
	 */
	public Clause DET			= new Clause(new DeterminerList());
	public Clause NOUN			= new Clause(new NounList());
	public Clause PRONOUN		= new Clause(new PronounList());
	public Clause VERB 			= new Clause(new VerbList());
	public Clause PREPOSITIONAL	= new Clause(new PrepositionalList());
	
	/**
	 * Grammar
	 */
	public Clause NOM = new Clause(Clause.GROUP), 
			NOM1 	= new Clause(Clause.CLAUSE), 
			NOM2  	= new Clause(Clause.CLAUSE), 
			NOM3  	= new Clause(Clause.CLAUSE); 
	public Clause PP = new Clause(Clause.GROUP), 
			PP1 	= new Clause(Clause.CLAUSE);
	public Clause NP = new Clause(Clause.GROUP), 
			NP1 	= new Clause(Clause.CLAUSE), 
			NP2 	= new Clause(Clause.CLAUSE); 
	public Clause VP = new Clause(Clause.GROUP), 
			VP1 	= new Clause(Clause.CLAUSE), 
			VP2 	= new Clause(Clause.CLAUSE), 
			VP3 	= new Clause(Clause.CLAUSE), 
			VP4 	= new Clause(Clause.CLAUSE), 
			VP5 	= new Clause(Clause.CLAUSE); 
	public Clause S = new Clause(Clause.GROUP), 
			S1		= new Clause(Clause.CLAUSE), 
			S2		= new Clause(Clause.CLAUSE);
	
	/**
	 * Private constructor ensures singleton and builds grammar
	 */
	public Grammar()
	{
		S1.addAll(NP, VP);
		S2.addAll(VP);
		S.addAll(S1);
		S.addAll(S1, S2);
		
		
		NP1.addAll(PRONOUN);
		NP2.addAll(DET, NOM);
		NP.addAll(NP1, NP2);
		
		NOM1.addAll(NOUN);
		NOM2.addAll(NOM, NOUN);
		NOM3.addAll(NOM, PP);
		NOM.addAll(NOM1, NOM2, NOM3);
		
		VP1.addAll(VERB);
		VP2.addAll(VERB, NP);
		VP.addAll(VP1);
		
		PP1.addAll(PREPOSITIONAL, NP);
		PP.addAll(PP1);
	}
	
	public Clause get()
	{
		return new Clause(Clause.GROUP, S);
	}
}
