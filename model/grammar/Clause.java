package model.grammar;

import java.util.Arrays;
import java.util.LinkedList;

import model.grammar.feature.AttributeValueMatrix;
import model.wordlist.WordList;

public class Clause implements Cloneable{
	
	public static final int CLAUSE = 0;
	public static final int GROUP = 1;
	public static final int LEXICON = 2;
	
	/**
	 * List of elements in clause
	 */
	private LinkedList<Clause> elements;
	private WordList lexicon;
	
	private int type;
	private AttributeValueMatrix avm;
	
	private Clause()
	{
		
	}

	/**
	 * Constructor method
	 * @param name		Clause name
	 * @param elements	Ordered list of elements in clause
	 */
	public Clause(int type, Clause... elements)
	{
		this.type = type;
		this.elements = new LinkedList<Clause>(Arrays.asList(elements));
	}
	
	public Clause(WordList lexicon)
	{
		this.type = LEXICON;
		this.lexicon = lexicon;
		this.avm = lexicon.getAvm();
	}
	
	/**
	 * Add method for clause element
	 * @param e		Element to be added
	 */
	public void add(Clause e)
	{
		this.elements.add(e);
	}
	
	public void addAll(Clause... e)
	{
		this.elements.addAll(Arrays.asList(e));
	}
	
	/**
	 * Replace 
	 * @param i
	 * @param e
	 */
	public void replace(int i, Clause... a)
	{
		this.elements.remove(i);
		this.elements.addAll(i, Arrays.asList(a));
	}
	
	public void replace(int i, LinkedList<Clause> a)
	{
		this.elements.remove(i);
		this.elements.addAll(i, a);
	}
	
	/**
	 * Get method for elements
	 * @return	LinkedList of elements
	 */
	public LinkedList<Clause> getElements()
	{
		return elements;
	}
	
	/**
	 * Get method for clause item
	 * @param index		Index of clause item
	 * @return			Clause item at index
	 */
	public Clause get(int index)
	{
		return elements.get(index);
	}
	
	public int getSize()
	{
		return this.elements.size();
	}
	
	/**
	 * Method to check if any more remains
	 * @return	True iff empty
	 */
	public boolean empty()
	{
		return elements.size() == 0;
	}
	
	public int getType()
	{
		return this.type;
	}
	
	public WordList getLexicon()
	{
		return lexicon;
	}
	
	public Clause clone()
	{
		Clause clause = new Clause();
		if(this.lexicon != null)
		{
			clause.lexicon = this.lexicon;
			clause.avm = this.avm;
		}
		if(this.elements != null) {
			clause.elements = new LinkedList<Clause>();
			this.elements.forEach(c -> clause.elements.add(c.clone()));
		}
		clause.type = this.type;
		return clause;
	}
	
	public AttributeValueMatrix getAvm()
	{
		return this.avm;
	}
	
	public void setAvm(AttributeValueMatrix avm)
	{
		this.avm = avm;
	}
	public boolean unify(Clause c)
	{
		boolean success = false;
		try{
			this.avm = this.avm.unify(c.avm);
			c.avm = this.avm;
			success = true;
		} catch(controller.exception.FeatureTypesDontMatchException fe)
		{
			success = false;
		} catch(controller.exception.InvalidFeatureValueTypeException ie)
		{
			success = false;
		}
		return success;
	}
}
