This repository contains the Java implementation of the core mechanisms of a machine learning enabled poetry generator: EvoPoem, which is described in the thesis "EvoPoem: Context Free Grammars for Automated Poetry"

## Research description ##
In the thesis for which this code was written, we discuss various poetry generators. We present our own text synthesis and natural language generation algorithm that can be used with various machine learning technologies, as well as a setup for the final machine learning enabled poetry generator, which we call EvoPoem. The algorithm is able to produce short, grammatically correct sentences, and create a visual spacing that suggests rhythm. This algorithm uses a grammar, a lexicon and a feature and unification algorithm enriched with constraint satisfaction, to parse a string of bits deterministically into a potential poem.

This program is not an implementation of the final machine learning enabled poetry generator, but implements the core mechanisms of converting a bitstring to a poem deterministically.

The current implementation is in Java and serves as a proof of concept only.


This source code is available free of charge for reference and reuse under the General Public License v3.0.